# 課題4.3: namespace のリソースを制限する

先の演習では、特定のDeployment に対して制限を設定しました。namespace 全体に制限を設定することも可能です。

**1. まずlow-usage-limit という新しいnamespace を作成し、作成されたことを確認します。**
```
student@setX-cp:~$ kubectl create namespace low-usage-limit

namespace/low-usage-limit created 
```
```
student@setX-cp:~$ kubectl get namespace  

NAME              STATUS   AGE
default           Active   1h
kube-node-lease   Active   1h
kube-public       Active   1h
kube-system       Active   1h
low-usage-limit   Active   42s
```

**2. CPU とメモリの使用量を制限するYAML ファイルを作成します。kind にはLimitRange を使用します。このファイルも、設定例を含むtar ファイル内にあるはずです。**
```
student@setX-cp:~$ vim low-resource-range.yaml
```
![chap4-3](/uploads/cf9437091a75452e0df5e23c186b5030/chap4-3.png)

[low-resource-range.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter4/labfiles/low-resource-range.yaml)


**3. LimitRange オブジェクトを作成し、先ほど作成したNamespace、low-usage-limit に割り当てます。namespace の指定には、--namespace もしくは-n を使用します。**
```
student@setX-cp:~$ kubectl --namespace=low-usage-limit create -f low-resource-range.yaml

limitrange/low-resource-range created
```

**4. 正常に実行できたことを確認します。なお、コマンドの実行にはnamespace の指定が必要であることを覚えておきましょう。特に指定しなければ、default namespace を利用します。**
```
student@setX-cp:~$ kubectl get LimitRange

No resources found in default namespace. 
```
```
student@setX-cp:~$ kubectl get LimitRange --all-namespaces  

NAMESPACE         NAME                 CREATED AT
low-usage-limit   low-resource-range   2023-04-18T01:19:31Z
```

**5. namespace 内に新しいDeployment を作成します。**
```
student@setX-cp:~$ kubectl -n low-usage-limit create deployment limited-hog --image vish/stress

deployment.apps/limited-hog created
```

**6. 現在のDeployment 一覧を表示します。hog は引き続きdefault のnamespace で起動しています。Calico ネットワークポリシーを使っている場合、次の出力より多くのDeployment が表示される可能性があります。**
```
student@setX-cp:~$ kubectl get deployments --all-namespaces

NAMESPACE         NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
default           hog                       1/1     1            1           7m57s  
kube-system       calico-kube-controllers   1/1     1            1           2d10h  
kube-system       coredns                   2/2     2            2           2d10h  
low-usage-limit   limited-hog               0/1     0            0           9s  
```

**7. low-usage-limit namespace 内のすべてのPod を表示します。namespace 内のリソースの補完にはtab キーの補完機能が使用できます。default namespace 以外のリソースをtab 補完するには、まず対象のnamespace を入力しましょう。**
```
student@setX-cp:~$ kubectl -n low-usage-limit get pods

NAME                           READY   STATUS    RESTARTS   AGE
limited-hog-2556092078-wnpnv   1/1     Running   0          2m11s
```

**8. Pod の詳細を確認しましょう。Pod はnamespace 全体の設定を継承しています。先にnamespace を指定すれば、シェルの補完機能が利用できます。**
```
student@setX-cp:~$ kubectl -n low-usage-limit get pod limited-hog-2556092078-wnpnv -o yaml

< 省略>
spec:
  containers:
  - image: vish/stress
    imagePullPolicy: Always
    name: stress
    resources:
      limits:
        cpu: "1"
        memory: 500Mi
      requests:
        cpu: 500m
        memory: 100Mi
    terminationMessagePath: /dev/termination-log
< 省略>
```

**9. 元のhog ファイルのために設定ファイルをコピーし、修正しましょう。新しいDeployment がlow-usage-limit のnamespaceに置かれるように、「namespace:」から始まる行を追加します。selfLink 行が存在する場合、削除しましょう。**
```
student@setX-cp:~$ cp hog.yaml hog2.yaml
student@setX-cp:~$ vim hog2.yaml
```
![chap4-4](/uploads/e176c95e325440bf7d57b290dfa1ef5a/chap4-4.png)

[hog2.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter4/labfiles/hog2.yaml)


**10. すべてのノードで追加のターミナルセッションを開き、top を実行します。新しくDeployment を作成したとき、まだ負荷の少ないノードにスケジュールされているでしょう。**

新たなDeployment を作成します。  
```
student@setX-cp:~$ kubectl create -f hog2.yaml

deployment.apps/hog created 
```

**11. Deployment の一覧を表示します。hog という同じ名前のDeployment が2 つ、それぞれ異なるnamespace で起動していることに注目しましょう。**
```
student@setX-cp:~$ kubectl get deployments --all-namespaces

NAMESPACE         NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
default           hog                       1/1     1            1           24m
kube-system       calico-kube-controllers   1/1     0            0           4h  
kube-system       coredns                   2/2     2            2           4h  
low-usage-limit   hog                       1/1     1            1           26s  
low-usage-limit   limited-hog               1/1     1            1           5m11s
```

**12. 他のターミナルで実行中のtop の出力内容を確認します。メモリの割り当てが完了した時点で、両方のhog Deployment がほぼ同じ量のリソースを利用しているはずです。各Deployment ごとの設定がnamespace のグローバルな設定を上書きしています。各ノードで次のような内容を出力しているでしょう。これはプロセッサー1 つと合計8G のメモリのうち12% を利用していることを示しています。**
<br>
<br>
25128 root 20 0 958532 954672 3180 R 100.0 11.7 0:52.27 stress  
24875 root 20 0 958532 954800 3180 R 100.3 11.7 41:04.97 stress  

**13. システムリソースを解放するため、2 つのDeployment hog を削除します。**
```
student@setX-cp:~$ kubectl -n low-usage-limit delete deployment hog

deployment.apps "hog" deleted
```
```
student@setX-cp:~$ kubectl delete deployment hog

deployment.apps "hog" deleted
```


以上  

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)