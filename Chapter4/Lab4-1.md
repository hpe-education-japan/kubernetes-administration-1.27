# 課題4.1: 基本的なノードのメンテナンス

この節では、etcd データベースをバックアップし、コントロールプレーンノードおよびWorker ノードのKubernetes のバージョンを更新します。  

**[etcd データベースをバックアップ]**

アップグレードの手法は安定化してきたものの、引き続きアップグレードの前にクラスタの状態をバックアップしておくのが良いでしょう。マーケットにはetcd のバックアップや管理に利用可能なツールが多く存在し、明確なバックアップおよびリストアの手法を提供しています。ここでは標準のスナップショットコマンドを利用しますが、リストアの方法は使うツールやクラスタのバージョン、復旧のもととなる故障状態に依存することに注意してください。  

**1. etcd デーモンのデータディレクトリを探します。Pod の設定はマニフェストにて確認できます。**
```
student@setX-cp:~$ sudo grep data-dir /etc/kubernetes/manifests/etcd.yaml

  - --data-dir=/var/lib/etcd
```

**2. etcd コンテナにログインし、etcdctl が提供するオプションを確認します。コンテナ名の補完には、tab キーを利用してください。**
```
student@setX-cp:~$ kubectl -n kube-system exec -it etcd-<Tab> -- sh
```

**etcd コンテナ内で操作**

**(a) etcdctl コマンドの引数とオプションを確認します。少し時間をかけて利用可能なオプションや引数を確認しましょ
う。Bourne shell では利用できないかもしれませんが、一度タイプしたあとは、コマンドと引数をコピー・ペーストし
たほうが簡単かもしれません。**
```
# etcdctl -h

NAME:  
         etcdctl - A simple command line client for etcd3.  
  
USAGE:  
         etcdctl [flags]  
 < 省略> 
```
  
**(b) TLS を利用するために、etcdctl コマンドに指定すべき3 つのファイルを探しましょう。ディレクトリに移動し、存在
するファイルを参照してください。新しいバージョンのetcd イメージは最小化されています。その結果、find コマンドがもはや利用できないかもしれません。/etc/kubernetes/pki/etcdというパスを覚えておく必要があります。ls コマンドが存在しないため、代わりにecho を使ってファイルを確認します。**
```
# cd /etc/kubernetes/pki/etcd
# echo *

ca.crt ca.key healthcheck-client.crt healthcheck-client.key
peer.crt peer.key server.crt server.key
```

**(c) 利用できるコマンドに制限のあるシェルでは特に、環境変数を利用することで、各鍵ファイルの名前の入力を省略することができます。コンテナのシェルからログアウトし、必要なファイルそれぞれへのパスを指定します。**
```
# exit
```
**コンテナ内ここまで**  
<br>
<br>


**3. ループバックIP とポート2379 を利用して、データベースの状態を確認します。サーバ証明書と鍵、およびCA 証明書を指定する必要があります。テキストにはコメントが記載されていますが、以下はそのままコピーして実行可能なように変更してあります。etcdのPod名を変更して実行してください。**
```
student@setX-cp:~$ kubectl -n kube-system exec -it etcd-setXX-cp -- sh \
-c "ETCDCTL_API=3 \
ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
etcdctl endpoint health"

https://127.0.0.1:2379 is healthy: successfully committed proposal: took = 11.942936ms 
```

**4. クラスタに属するetcd データベースの数を確認します。50% より多くのクォラムを確保するため、3 台や5 台がProduction 環
境ではよく利用されます。今回の演習では 1 台のみの構成を利用します。上矢印キーを押して前のコマンドを表示し、編集すれば、コマンド全体を再び入力する必要はありません。**
```
student@setX-cp:~$ kubectl -n kube-system exec -it etcd-setXX-cp -- sh \
-c "ETCDCTL_API=3 \
ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
etcdctl --endpoints=https://127.0.0.1:2379 member list"


 fb50b7ddbf4930ba, started, master, https://10.128.0.35:2380, https://10.128.0.35:2379, false 
```

**5. -w オプションを追加で指定することで、クラスタの状態を、表形式で確認することもできます。ここでも、上矢印キーを押すことで、前のコマンドの末尾のみを簡単に修正することができます。**
```
student@setX-cp:~$  kubectl -n kube-system exec -it etcd-setXX-cp -- sh \
-c "ETCDCTL_API=3 \
ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
etcdctl --endpoints=https://127.0.0.1:2379 member list -w table"

+------------------+---------+-------+--------------------------+--------------------------+------------+
| ID               | STATUS  | NAME  | PEER ADDRS               | CLIENT ADDRS             | IS LEARNER |
+------------------+---------+-------+--------------------------+--------------------------+------------+
| 802d78549985d5a8 | started | k8scp | https://10.128.0.15:2380 | https://10.128.0.15:2379 | false      |
+------------------+---------+-------+--------------------------+--------------------------+------------+
```

**6. etcd データベースの台数が確認できたので、その状態を確認し、バックアップを取得します。引数にsnapshot を指定し、スナップショットをコンテナ内のデータディレクトリ/var/lib/etcd/に保存します。**
```
student@setX-cp:~$  kubectl -n kube-system exec -it etcd-setXX-cp -- sh \
-c "ETCDCTL_API=3 \
ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
etcdctl --endpoints=https://127.0.0.1:2379 \
snapshot save /var/lib/etcd/snapshot.db "

{"level":"info","ts":1598380941.6584022,"caller":"snapshot/v3_snapshot.go:110","msg":"created temporary db file","path":"/var/lib/etcd/snapshot.db.part"}  
{"level":"warn","ts":"2020-08-25T18:42:21.671Z","caller":"clientv3/retry_interceptor.go:116","msg":"retry stream intercept"}  
{"level":"info","ts":1598380941.6736135,"caller":"snapshot/v3_snapshot.go:121","msg":"fetching snapshot","endpoint":"https://127.0.0.1:2379"}  
{"level":"info","ts":1598380941.7519674,"caller":"snapshot/v3_snapshot.go:134","msg":"fetched snapshot","endpoint":"https://127.0.0.1:2379","took":0.093466104}  
{"level":"info","ts":1598380941.7521122,"caller":"snapshot/v3_snapshot.go:143","msg":"saved","path":"/var/lib/etcd/snapshot.db"}  
Snapshot saved at /var/lib/etcd/snapshot.db 
``` 

**7. ノード上にスナップショットが存在することを確認します。ファイルの更新時刻が少し前の時間になっているべきです。**
```
student@setX-cp:~$ sudo ls -l /var/lib/etcd/

 total 3888  
 drwx------ 4 root root 4096    Aug 25 11:22 member  
 -rw------- 1 root root 3973152 Aug 25 18:42 snapshot.db 
```

**8. スナップショットとクラスタの構築に利用した情報を、ローカルにバックアップします。またノードが利用できなくなった場合に備えて、他のシステムにもバックアップしましょう。**
<br>
<br>
すぐにリストアできるようにするため、cronjob 等を利用して定期的にスナップショットを取得するように留意してください。snapshot restore を使う際は、データベースが利用されていないことが重要です。
HA クラスタでは、コントロールプレーンノードは除去・置換されるため、リストアは不要です。リストア方法の詳細は、ここに記載されています: https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#restoring-an-etcd-cluster 

```
student@setX-cp:~$ mkdir $HOME/backup
student@setX-cp:~$ sudo cp /var/lib/etcd/snapshot.db $HOME/backup/snapshot.db-$(date +%m-%d-%y)
student@setX-cp:~$ sudo cp /root/kubeadm-config.yaml $HOME/backup/
student@setX-cp:~$ sudo cp -r /etc/kubernetes/pki/etcd $HOME/backup/
```

**9. リストア中に誤りがあると、クラスタが利用できなくなる場合があります。問題を起こしてクラスタを再構築することにな
らないように、演習の最後にデータベースのリストアを試してみて下さい。リストア手順の詳細はここで確認できます。**

https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#restoring-an-etcd-cluster

<br>
<br>

**[クラスタをアップグレード]**

**1. まず、レポジトリを1.28用に変更して、APT のパッケージメタデータを更新します。**
```
 student@setX-cp:~$ sudo vim /etc/apt/sources.list.d/kubernetes.list

1.27を1.28に書き換え

deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /
```
```
student@setX-cp:~$ sudo apt update

Hit:1 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal InRelease
Hit:2 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal-updates InRelease
Hit:3 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal-backports InRelease
Hit:4 https://download.docker.com/linux/ubuntu focal InRelease

<省略>
```

**2. 利用可能なパッケージを確認します。一覧が長くなるため、上下にスクロールして最近のバージョンを探します。現在インストールされているバージョンの次のフルバージョンを選択してください。例えば、前の演習で1.24.1 を使ってクラスタを構築した場合は、1.25.1 を選択します。1.27.1 を使った場合は、1.28.1 を選択します。1.29.1 を使った場合は、1.30.1 を選択することになります。"madison" という用語が使われているのは、madison と呼ばれる Debian のツールが基となっています。**
```
student@setX-cp:~$ sudo apt-cache madison kubeadm

   kubeadm | 1.28.7-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.6-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.5-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.4-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.3-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.2-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.1-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.0-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
< 省略 >

```  

**3. kubeadm のバージョン固定を解除し、パッケージを更新します。。次のバージョンの 1 つめのアップデートに更新するように注意
して下さい。**
```
student@setX-cp:~$ sudo apt-mark unhold kubeadm

Canceled hold on kubeadm.
```
```
student@setX-cp:~$ sudo apt-get install -y kubeadm=1.28.1-1.1

Reading package lists... Done
Building dependency tree
Reading state information... Done
< 省略>
```

**4. 他のソフトウェアと一緒に更新されることを防ぐため、再びバージョンを固定します。**
```
student@setX-cp:~$ sudo apt-mark hold kubeadm

 kubeadm set on hold. 
```

**5. インストールされたKubeadm のバージョンを確認します。先ほどインストールした新しいバージョンが表示されるはずです。**
```
student@setX-cp:~$ sudo kubeadm version

kubeadm version: &version.Info{Major:"1", Minor:"28", GitVersion:"v1.28.1", GitCommit:"8dc49c4b984b897d423aab4971090e1879eb4f23", GitTreeState:"clean", BuildDate:"2023-08-24T11:21:51Z", GoVersion:"go1.20.7", Compiler:"gc", Platform:"linux/amd64"}

``` 
  
**6. コントロールプレーンノードをアップデートする準備のため、まずはじめに可能な限りPod を追い出す必要があります。Daemonset はその性質上全てのノードで稼働し、またCalico などは稼働し続ける必要があります。Daemonset はここでは無視します。**

```
student@setX-cp:~$ kubectl drain setX-cp --ignore-daemonsets

node/setX-cp cordoned
WARNING: ignoring DaemonSet-managed Pods: kube-system/calico-node-r6rkh, kube-system/kube-proxy-kngq6
evicting pod kube-system/calico-kube-controllers-5447dc9cbf-6d7nw
evicting pod kube-system/coredns-66bff467f8-brl45
evicting pod kube-system/coredns-66bff467f8-q5ms8
pod/calico-kube-controllers-5447dc9cbf-6d7nw evicted
pod/coredns-66bff467f8-brl45 evicted
pod/coredns-66bff467f8-q5ms8 evicted
node/setX-cp drained
```

**7. upgrade plan パラメータを指定し、既存のクラスタをチェックした上で、ソフトウェアをアップデートします。利用している.1 アップデートよりも新しいバージョンが利用か可能かもしれませんが。前の演習でクラスタを 1.25.1 を使って構築した場合は1.26.1 を利用します。1.26.1 を使って構築した場合は 1.27.1 に更新します。出力を確認し、アップデートによる変更を確認しましょう。**
```
student@setX-cp:~$ sudo kubeadm upgrade plan

[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade] Fetching available versions to upgrade to
[upgrade/versions] Cluster version: v1.27.1
[upgrade/versions] kubeadm version: v1.28.1
I0315 16:53:18.385743    6381 version.go:256] remote version is much newer: v1.29.2; falling back to: stable-1.28
[upgrade/versions] Target version: v1.28.7

< 省略 >

```
  
**8. アップグレードを実際に実行する準備ができました。非常に多くの内容が出力されます。アップグレードを継続するかを尋ねられることに注意し、、yes に相当する y を入力します。しばらく経った後、エラーやetcd やその他パッケージのバージョンの更新といった提示された内容を確認しましょう。**
```
student@setX-cp:~$ sudo kubeadm upgrade apply v1.28.1

[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade/version] You have chosen to change the cluster version to "v1.28.1"
[upgrade/versions] Cluster version: v1.27.1
[upgrade/versions] kubeadm version: v1.28.1
[upgrade] Are you sure you want to proceed? [y/N]: y

< 省略 >

[upgrade/successful] SUCCESS! Your cluster was upgraded to "v1.28.1". Enjoy!

[upgrade/kubelet] Now that your control plane is upgraded, please proceed with upgrading your kubelets if you haven't already done so.

```
   
**9. projectcalico.org やその他のCNI について、更新に対応するバージョンを確認します。apply コマンドが何かしらの情報を表示する場合がありますが、Kubernetes に直接関連しないプロジェクトについても、新しいバージョンに対応するよう、アップデートする必要があります。**

**10. ノードの状態を確認します。コントロールプレーンノードはスケジューリングが無効化されているべきです。加えて、一部のソフトウェアが更新されておらず、またデーモンが再起動されていないため、古いバージョンが表示されます。**
```
student@setX-cp:~$ kubectl get node

NAME          STATUS                     ROLES           AGE    VERSION
set0-cp       Ready,SchedulingDisabled   control-plane   103m   v1.27.11
set0-worker   Ready                      <none>          102m   v1.27.11
```

**11. kubelet とkubectl の固定を解除します。**
```
student@setX-cp:~$ sudo apt-mark unhold kubelet kubectl

 Canceled hold on kubelet.  
 Canceled hold on kubectl. 
```

**12. 両方のパッケージを、kubeadm と同じバージョンに更新します。**
```
student@setX-cp:~$ sudo apt-get install -y kubelet=1.28.1-1.1 kubectl=1.28.1-1.1

Reading package lists... Done
Building dependency tree
Reading state information... Done

< 省略>

```
  
**13. 他の更新操作によって更新されないよう、Kubernetes に関連するソフトウェアを再び固定します。**
```
student@setX-cp:~$ sudo apt-mark hold kubelet kubectl

 kubelet set on hold.  
 kubectl set on hold.
```

**14. デーモンを再起動します。**
```
student@setX-cp:~$ sudo systemctl daemon-reload
student@setX-cp:~$ sudo systemctl restart kubelet
```
**15. コントロールプレーンノードが新しいバージョンに更新されたことを確認します。その後、他のコントロールプレーンノードを同じ手順でアップデートします。ただし、sudo kubeadm upgrade apply ではなくsudo kubeadm upgrade node を実行してください。**
```
student@setX-cp:~$ kubectl get node

NAME          STATUS                     ROLES           AGE    VERSION
set0-cp       Ready,SchedulingDisabled   control-plane   107m   v1.28.1
set0-worker   Ready                      <none>          105m   v1.27.11
```

**16. コントロールプレーンノードをスケジュール可能にします。ノード名は実際の環境のノード名に一致するように変更して下さい。**
```
student@setX-cp:~$ kubectl uncordon setX-cp

node/setX-cp uncordoned
```

**17. コントロールプレーンノードのステータスがReady と表示されることを確認します。**
```
student@setX-cp:~$ kubectl get node

NAME          STATUS   ROLES           AGE    VERSION
set0-cp       Ready    control-plane   108m   v1.28.1
set0-worker   Ready    <none>          106m   v1.27.11
```

**18. クラスタのWorker ノードをアップデートします。Worker ノードに対する2 つめのターミナルセッションを用意してください。いくつかのコマンドをコントロールプレーンノードでも実行する必要があるため、2 つセッションを用意すると便利です。まずはじめにWorker ノードでのソフトウェア更新を許可します。**
```
student@setX-worker:~$ sudo apt-mark unhold kubeadm

Canceled hold on kubeadm. 
```

**19. リポジトリを更新して、kubeadm パッケージをコントロールプレーンノードと同じバージョンに更新します。**
```
 student@setX-cp:~$ sudo vim /etc/apt/sources.list.d/kubernetes.list

1.27を1.28に書き換え

deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /


student@setX-worker:~$ sudo apt-get update && sudo apt-get install -y kubeadm=1.28.1-1.1

< 省略>

Setting up kubeadm (1.28.1-1.1) ...
```
  
**20. パッケージを再度固定します。**
```
student@setX-worker:~$ sudo apt-mark hold kubeadm

kubeadm set on hold. 
```

**21. コントロールプレーンノードのターミナルセッションに戻り、Worker ノードをdrain します。ただし、Daemonset はそのまま残します。**
```
student@setX-cp:~$ kubectl drain setX-worker --ignore-daemonsets

node/setX-worker cordoned
WARNING: ignoring DaemonSet-managed Pods: kube-system/calico-node-nwm8w, kube-system/kube-proxy-66sh2
evicting pod default/before-688b465898-2sdx7
evicting pod default/after-6967f9db5-ws852
evicting pod kube-system/calico-kube-controllers-5447dc9cbf-5j947
< 省略>

pod/coredns-66bff467f8-nxclf evicted
pod/calico-kube-controllers-5447dc9cbf-5j947 evicted
node/setX-worker drained
```

**22. Worker ノードに戻り、更新されたノードの設定をダウンロードします。**
```
student@setX-worker:~$ sudo kubeadm upgrade node

[upgrade] Reading configuration from the cluster...
[upgrade] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks
[preflight] Skipping prepull. Not a control plane node.
[upgrade] Skipping phase. Not a control plane node.
[upgrade] Backing up kubelet config file to /etc/kubernetes/tmp/kubeadm-kubelet-config1674290445/config.yaml
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[upgrade] The configuration for this node was successfully updated!
[upgrade] Now you should go ahead and upgrade the kubelet package using your package manager.
```
  
**23. ソフトウェアの固定を解除し、コントロールプレーンノードと同じバージョンに更新します。**
```
student@setX-worker:~$ sudo apt-mark unhold kubelet kubectl

Canceled hold on kubelet.  
Canceled hold on kubectl. 
```
```
student@setX-worker:~$ sudo apt-get install -y kubelet=1.28.1-1.1 kubectl=1.28.1-1.1

Reading package lists... Done
Building dependency tree

< 省略>

Setting up kubectl (1.28.1-1.1) ...
Setting up kubelet (1.28.1-1.1) ...
```
  
**24. 通常のアップデートにてパッケージが更新されないようにします。**
```
student@setX-worker:~$ sudo apt-mark hold kubelet kubectl

kubelet set on hold.  
kubectl set on hold.
```

**25. デーモンプロセスを再起動し、更新されたソフトウェアを反映します。**
```
student@setX-worker:~$ sudo systemctl daemon-reload
student@setX-worker:~$ sudo systemctl restart kubelet
```

**26. コントロールプレーンノードに戻り、ノードの状態を確認します。Worker ノードのステータスに注目してください。**
```
student@setX-cp:~$ kubectl get node

NAME          STATUS                     ROLES           AGE    VERSION
set0-cp       Ready                      control-plane   114m   v1.28.1
set0-worker   Ready,SchedulingDisabled   <none>          113m   v1.28.1
```
  
**27. Pod をWorker ノード上に配置可能にします。**
```
student@setX-cp:~$ kubectl uncordon setX-worker

node/setX-worker uncordoned
```
  
**28. 両方のノードがReady ステータスと表示されることを確認します。**
```
student@setX-cp:~$ kubectl get nodes

NAME          STATUS   ROLES           AGE    VERSION
set0-cp       Ready    control-plane   115m   v1.28.1
set0-worker   Ready    <none>          114m   v1.28.1
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter4/Lab4-2.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/tree/main?ref_type=heads)
