# 課題15.1: TLS の利用

**概要**

クラスタへのアクセスの流れはTLS 接続から始まり、認証に続き認可、そして最後にAdmission Control プラグインが高度な機能を許可したらリクエストを実行するということを学びました。Initializers の利用により、柔軟性のあるシェルスクリプトによるリクエストの動的な修正が可能になります。セキュリティは重要かつ継続的な関心事であるため、クラスタの必要に応じて複数の設定を利用する可能性があります。  
クラスタにAPI リクエストを送信するすべてのプロセスは認証される必要があります。もし認証されなければ、匿名ユーザとして扱われます。  

1 つのクラスタに複数のルート認証局(CA) を設置することも可能ですが、デフォルトでは各クラスタにCA が1 つあり、それをクラスタ間通信に利用します。CA 証明書を各ノードに配布し、またデフォルトのServiceAccount に付随するSecret として配布します。kubelet は、ローカルコンテナが正常に起動していることを保証するローカルエージェントです。  

**0. ホームディレクトリに戻っておきましょう。**
```
student@setX-cp:~$ cd ~
```

**1. コントロールプレーンノードとセカンダリノードでkubelet の状態を確認しましょう。kube-apiserver も証明書や認可モードなどのセキュリティ情報が表示されます。kubelet はsystemd のサービスであるため、その出力を見ていきます。**
```
student@setX-cp:~$ systemctl status kubelet.service

● kubelet.service - kubelet: The Kubernetes Node Agent
   Loaded: loaded (/lib/systemd/system/kubelet.service; enabled; vendor preset: en
  Drop-In: /etc/systemd/system/kubelet.service.d
        |__10-kubeadm.conf
< 省略> 
```

**2. ステータスの出力を見てください。cgroup およびkubelet の情報（設定内容を含む長い行）を追っていくと、設定ファイルが置かれている場所がわかるはずです。**

CGroup: /system.slice/kubelet.service  
&emsp;\|--19523 /usr/bin/kubelet .... --config=/var/lib/kubelet/config.yaml ..  

**3. /var/lib/kubelet/config.yamlファイル内の設定を確認しましょう。kube-apiserver にアクセスするために、/etc/kubernetes/pki/ ディレクトリを利用していることがわかるはずです。また、出力の末尾あたりで、Pod の設定ファイルのパスも設定されています。**
```
student@setX-cp:~$ sudo less /var/lib/kubelet/config.yaml
```

![chap15-1](/uploads/f561852610b2a4c05bba250a98790276/chap15-1.png)


**4. コントロールプレーンノード上の他のエージェントは、kube-apiserver と通信しています。これらの設定を行っているファイルを見てみましょう。**

先のYAML ファイルで設定しています。いずれかのファイルで証明書情報を確認しましょう。  
```
student@setX-cp:~$ sudo ls /etc/kubernetes/manifests/

etcd.yaml            kube-controller-manager.yaml
kube-apiserver.yaml  kube-scheduler.yaml
```
```
student@setX-cp:~$ sudo less /etc/kubernetes/manifests/kube-controller-manager.yaml

< 省略>
```

**5. 実施不可：コンポーネント間通信の認可は、基本的にトークンを利用するようになっています。トークンはSecret として保存されています。kube-system Namespace 内に存在するSecret を確認しましょう。（Kubernetes V1.26ではToken Secretは自動で作成されません）**
```
student@setX-cp:~$ kubectl -n kube-system get secrets

NAME                                             TYPE                                  DATA   AGE
attachdetach-controller-token-xqr8n              kubernetes.io/service-account-token   3      5d  
bootstrap-signer-token-xbp6s                     kubernetes.io/service-account-token   3      5d  
bootstrap-token-i3r13t                           bootstrap.kubernetes.io/token         7      5d  
< 省略> 
```

**6. 実施不可：Secret certificate-controller-token の詳細を見てみましょう。**
```
student@setX-cp:~$ kubectl -n kube-system get secrets certificate<Tab> -o yaml
```
![chap15-2](/uploads/645e598452e60fc183200b7125e24223/chap15-2.png)


**7. kubectl config コマンドは、パラメータの確認や設定更新に利用できます。現在の設定を確認しましょう。キーや証明書は機密情報なので、代わりに"REDACTED"(墨塗りという意味) と表示されています。**
```
student@setX-cp:~$ kubectl config view

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
< 省略>  
```

**8. オプションを確認しましょう。管理者のために、キーではなくパスワードを設定するオプションなどが存在します。出力内の例とオプションを読んでみましょう。**
```
student@setX-cp:~$ kubectl config set-credentials -h

Sets a user entry in kubeconfig
< 省略>
```

**9. kubectl コマンドの設定ファイルのコピーを作成しましょう。後述の手順でこのファイルを更新し、差分を見ていきます。**
```
student@setX-cp:~$ cp $HOME/.kube/config $HOME/cluster-api-config
```

**10. kubectl とkubeadm、それぞれを利用してクラスタやセキュリティの色々な設定を試してみましょう。クラスタ名などの値を見つけましょう。kubeadm を利用するには、root にならなければいけません。**
```
student@setX-cp:~$ kubectl config <Tab><Tab>

current-context get-contexts   set-context view
delete-cluster  rename-context set-credentials
delete-context  set            unset
get-clusters    set-cluster    use-context
```
```
student@setX-cp:~$ sudo kubeadm token -h

< 省略>
```
```
student@setX-cp:~$ sudo kubeadm config -h

< 省略>
```

**11. クラスタのデフォルトの設定を確認しましょう。クラスタのセキュリティやインフラについて面白い情報が見つかるかもしれません。**
```
student@setX-cp:~$ sudo kubeadm config print init-defaults

apiVersion: kubeadm.k8s.io/v1beta2
bootstrapTokens:
- groups:
  - system:bootstrappers:kubeadm:default-node-token
  token: abcdef.0123456789abcdef
  ttl: 24h0m0s
  usages:
  - signing
< 省略>  
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter15/Lab15-2.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
