# 課題9.1: 新規Service のデプロイ


**概要**

Service は、Pod の論理的なグループにアクセスするためのポリシーを定義するオブジェクトです。これを用いてマイクロサービスを作成することができます。フロントエンドやバックエンドのコンテナを削除・置換する際、対応するリソースへの永続的なアクセスを可能にするため、Service は通常、Label を用いてリソースを割り当てます。  

Kubernetes ネイティブアプリケーションはEndpoints API を利用してPod にアクセスできます。Kubernetes ネイティブではないアプリケーションは、仮想IP アドレスベースのブリッジを利用してバックエンドPod にアクセスできます。ServiceType には次の種類が存在します。 

• ClusterIP: デフォルトのServiceType です。クラスタ内部のIP アドレスでサービスやアプリを公開します。クラスタ内からのみアクセス可能です。  
• NodePort: ノードのIP アドレスを静的ポートで公開します。ClusterIP も自動的に作成します。  
• LoadBalancer: クラウドプロバイダのロードバランサを利用してService を外部に公開します。NodePort とClusterIP も自動的に作成します。  
• ExternalName: CNAME レコードを利用してService をExternalName の引数にマッピングします。  

Service を利用してアクセス先と実際の処理を行うPod を切り離すことで、クライアントからバックエンドアプリケーションへのアクセスを中断することなく、すべてのエージェントもしくはオブジェクトの置き換えができるようになります。  

**1. nginx サーバを2 つデプロイするyamlファイルを作成し、kubectl を実行しましょう。**

kind はDeployment とし、nginx というLabel を付けましょう。レプリカを2 つ作成し、8080 番ポートを公開します。次の例には多くの解説コメントを含めています。ファイルを作成する際にはこのコメントを含む必要はありません。このファイルもtar ファイル内にあるはずです。  

```
student@setX-cp:~$ vim nginx-one.yaml
```
![chap9-4](/uploads/2cc1cde8f5ee709e95dd1813f0bf8da2/chap9-4.png)

[nginx-one.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter9/labfiles/nginx-one-0.yaml)

 
**2. クラスタ内のノードのLabel を一覧表示してみましょう。**
```
student@setX-cp:~$ kubectl get nodes --show-labels

<省略>
```

**3. 次のコマンドを実行し、エラーの原因を探してみましょう。accounting namespace が見つからないというエラーが表示されるはずです。**
```
student@setX-cp:~$ kubectl create -f nginx-one.yaml

Error from server (NotFound): error when creating "nginx-one.yaml": namespaces "accounting" not found
```  

**4. accounting Namespace を作成し、Deployment を再作成してみましょう。今回はエラーは起きないはずです。**
```
student@setX-cp:~$ kubectl create ns accounting

namespace/accounting" created
```
```
student@setX-cp:~$ kubectl create -f nginx-one.yaml

deployment.apps/nginx-one created
```

**5. Pod のステータスを確認しましょう。Running 状態ではないはずです。**
```
student@setX-cp:~$ kubectl -n accounting get pods

NAME                        READY   STATUS    RESTARTS   AGE
nginx-one-74dd9d578d-fcpmv  0/1     Pending   0          4m  
nginx-one-74dd9d578d-r2d67  0/1     Pending   0          4m 
```

**6. Pod の詳細を確認してみましょう。出力の最後尾にあるEvents に、Pod がNode に割り当てられていない理由が表示されています。**
```
student@setX-cp:~$ kubectl -n accounting describe pod nginx-one-74dd9d578d-fcpmv

Name:           nginx-one-74dd9d578d-fcpmv
Namespace:      accounting
Node:           <none>

< 省略>
Events:
  Type     Reason            Age                From               Message
  ----     ------            ----               ----               -------
  Warning  FailedScheduling  27s (x2 over 28s)  default-scheduler  0/2 nodes are available: 2 node(s) didn't match Pod's node affinity/selector.
```

**7. セカンダリノードにLabel を付与します。ノード名に注意してください。Labelの値は大文字と小文字を区別することに注意してください。Label が付与されていることを確認しましょう。**
```
student@setX-cp:~$ kubectl label node setX-worker system=secondOne

node/worker labeled
```
```
student@setX-cp:~$ kubectl get nodes --show-labels

NAME     STATUS   ROLES                  AGE   VERSION   LABELS
k8scp    Ready    control-plane,master   15h   v1.24.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=k8scp,kubernetes.io/os=linux,node-role.kubernetes.io/control-plane=,node-role.kubernetes.io/master=,node.kubernetes.io/exclude-from-external-load-balancers=
worker   Ready    <none>                 15h   v1.24.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=worker,kubernetes.io/os=linux,system=secondOne
```

**8. accounting Namespace のPod を一覧表示してみましょう。まだPending 状態のものがあるかもしれません。Deployment を作成してからあまり時間が経っていないと、設定したLabel の値をシステムがまだ確認していない場合があるからです。1 分経ってもPod がPending と表示されたままの場合、Pod を1 つ削除してみましょう。Pod を削除した後に再確認すると、2 つのPod がRunning 状態になっているはずです。状態を変更したことにより、Deployment コントローラが両方のPod の状態を確認したからです。**
```
student@setX-cp:~$ kubectl -n accounting get pods

NAME                        READY   STATUS    RESTARTS   AGE
nginx-one-74dd9d578d-fcpmv  1/1     Running   0          10m  
nginx-one-74dd9d578d-sts5l  1/1     Running   0          3s  
```

**9. YAML ファイルで設定したLabel を利用してPod を一覧表示してみましょう。Pod にはsystem=secondary というLabel を付けました。**
```
student@setX-cp:~$ kubectl get pods -l system=secondary --all-namespaces

NAMESPACE    NAME                        READY   STATUS    RESTARTS   AGE
accounting   nginx-one-74dd9d578d-fcpmv  1/1     Running   0          20m  
accounting   nginx-one-74dd9d578d-sts5l  1/1     Running   0          9m  
```

**10. YAML ファイルでは8080 番ポートを公開しました。新しいDeployment を公開してみましょう。**
```
student@setX-cp:~$ kubectl -n accounting expose deployment nginx-one

service/nginx-one exposed
```

**11. 公開したエンドポイントを確認しましょう。各Pod の8080 番ポートが公開されたことを確認します。**
```
student@setX-cp:~$ kubectl -n accounting get ep nginx-one

NAME        ENDPOINTS                             AGE
nginx-one   192.168.1.72:8080,192.168.1.73:8080   47s
```

**12. まず8080 番ポートで、次に80 番ポートで、Pod へのアクセスを試してみましょう。コンテナの8080 番ポートは公開済みですが、中のアプリケーションはまだこのポートを待ち受けるように設定していません。nginx サーバはデフォルトでは80 番ポートを待ち受けます。そのポートへのcurl コマンドで一般的なウェルカムページを表示できます。**
```
student@setX-cp:~$ curl 192.168.1.72:8080

curl: (7) Failed to connect to 192.168.1.72 port 8080: Connection refused
```
```
student@setX-cp:~$ curl 192.168.1.72:80

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
< 省略>  
```

**13. Deployment を削除し、80 番ポートを公開するようにYAML ファイルを修正して、Deployment を再作成しましょう。**
```
student@setX-cp:~$ kubectl -n accounting delete deploy nginx-one

deployment.apps "nginx-one" deleted
```
```
student@setX-cp:~$ vim nginx-one.yaml
```

![chap9-2](/uploads/30dce42d9ab9ef7dbb6ef39d61266b47/chap9-2.png)

[nginx-one.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter9/labfiles/nginx-one-1.yaml)

```
student@setX-cp:~$ kubectl create -f nginx-one.yaml

deployment.apps/nginx-one created
```
  

以上  

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter9/Lab9-2.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
