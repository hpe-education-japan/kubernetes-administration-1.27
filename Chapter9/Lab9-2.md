# 課題9.2: NodePort の設定

先の課題では、ClusterIP を自動的にデプロイするkubectl expose を利用しました。この課題では、NodePort をデプロイします。NodePort がなくてもクラスタ内からはコンテナにアクセスできますが、NodePort を利用することでクラスタ外からトラフィックをNATすることができます。LoadBalancer ではなくNodePort をデプロイすべき理由は、LoadBalancer はGKE やAWS のようなクラウドプロバイダのロードバランサリソースでもあるからです。  

**1. 先のステップでは、Pod の内部IP アドレスを利用してnginx のページを表示しました。今度は--type=NodePort を利用してDeployment を公開します。覚えやすい名前を付けてaccounting namespace にサービスをデプロイします。ファイアウォールの設定を容易にするために、ポート番号を指定することも可能です。**
```
student@setX-cp:~$ kubectl -n accounting expose deployment nginx-one --type=NodePort --name=service-lab

service/service-lab exposed
```

**2. accounting namespace 内のService の詳細を確認しましょう。自動生成されたポートを探してみましょう。**
```
student@setX-cp:~$ kubectl -n accounting describe services

....  
NodePort: <unset> 32103/TCP  
....  
```

**3. クラスタの外部ホスト名やIP アドレスを見つけましょう。この演習はFloatingIP を経由してアクセスするGCP ノードの利用を想定しているので、まずは内部からアクセス可能なIP アドレスを確認します。Kubernetes コントロールプレーンノードのURL を確認しましょう。内部と外部のIP アドレスの両方に対して、どちらでアクセスできるか確認します。**
```
student@setX-cp:~$ kubectl cluster-info

Kubernetes control plane is running at https://k8scp:6443
CoreDNS is running at https://k8scp:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

**4. コントロールプレーンノードのURL とNodePort を利用してnginx ウェブサーバへのアクセスを試してみましょう。**
```
student@setX-cp:~$ curl http://k8scp:32103

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
```

**5. ローカルマシンのブラウザを利用して、SSH 接続を行ったノードのバブリックIP アドレスとポート番号にアクセスしましょう。**

nginx のデフォルトページが表示されるはずです。curl を使ってパブリック IP を検出することもできます。  
```
student@setX-cp:~$ curl ifconfig.io

104.198.192.84
```


以上  

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter9/Lab9-3.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)

