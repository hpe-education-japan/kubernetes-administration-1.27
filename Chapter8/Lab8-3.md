# 課題8.3: Persistent Volume Claim (PVC) の作成

Pod が新しいPV を利用するためには、まずPersistent Volume Claim (PVC) を作成しなければなりません。  

**1. まず、既存のPVC があるかどうかを調べます。**
```
student@setX-cp:~$ kubectl get pvc

No resources found in default namespace.
```

**2. PVC を作成するためのYAML ファイルを作成しましょう。**
```
student@setX-cp:~$ vim pvc.yaml
```
![chap8-7](/uploads/61a1833e2ea088693971499eb6f77836/chap8-7.png)

[pvc.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/pvc.yaml)


**3. PVC を作成し、STATUS がBound になっていることを確認しましょう。サイズは200Mi を指定したにも関わらず、1Gi を利用しています。使用可能なPersistent Volume が1Gi のもののみだったためです。**
```
student@setX-cp:~$ kubectl create -f pvc.yaml

persistentvolumeclaim/pvc-one created
```
```
student@setX-cp:~$ kubectl get pvc

NAME      STATUS   VOLUME    CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-one   Bound    pvvol-1   1Gi        RWX                           4s
```

**4. PV が利用されているかどうかを確認するため、PV の状態を再確認しましょう。STATUS がBound になっているはずです。**
```
student@setX-cp:~$ kubectl get pv

NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM             STORAGECLASS   REASON   AGE
pvvol-1   1Gi        RWX            Retain           Bound    default/pvc-one                           5m
```

**5. PVC を利用する新しいDeployment を作成しましょう。以前の課題で作成したDeployment のYAML ファイルをコピーし、修正します。次のようにDeployment 名を変更し、一般的なspec に加え、containers: セクションの後半部分にvolumeMountsの設定を追加します。volume 名は任意ですが、spec.template.spec.containers.< コンテナ>.volumeMounts.name とspec.template.spec.volumes.< ボリューム>.name で一致させる必要があります。また、claimName に設定する値と同じ名前のPVC が存在しなければなりません。volumes 行は、containers 行とdnsPolicy 行と同じインデントです。**
```
student@setX-cp:~$ cp first.yaml nfs-pod.yaml

first.yamlが存在しない場合はそのまま以下を実行して、ファイルを作成してください。

student@setX-cp:~$ vim nfs-pod.yaml
```
![chap8-14](/uploads/6a985c7b37c7e7bd8009f4cbf3266da4/chap8-14.png)

[nfs-pod.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/nfs-pod.yaml)


**6. 修正したファイルを利用してPod を作成しましょう。**
```
student@setX-cp:~$ kubectl create -f nfs-pod.yaml

deployment.apps/nginx-nfs created
```

**7. Pod の詳細を確認しましょう。DaemonSet のPod も起動しているかもしれません。**
```
student@setX-cp:~$ kubectl get pods

NAME                         READY   STATUS    RESTARTS   AGE
nginx-nfs-1054709768-s8g28   1/1     Running   0          3m
```
```
student@setX-cp:~$ kubectl describe pod nginx-nfs-1054709768-s8g28

Name:         nginx-nfs-1054709768-s8g28n
Namespace:    default
Priority:     0
Node:         worker/10.128.0.5

< 省略>
    Mounts:
      /opt from nfs-vol (rw)
      
< 省略>
Volumes:
  nfs-vol:
    Type:       PersistentVolumeClaim (a reference to a PersistentV...
    ClaimName:  pvc-one
    ReadOnly:   false
< 省略>
```

**8. PVC の状態を一覧表示してみましょう。STATUS がBound になっているはずです。**
```
student@setX-cp:~$ kubectl get pvc

NAME      STATUS   VOLUME    CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-one   Bound    pvvol-1   1Gi        RWX                           2m
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/Lab8-4.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
