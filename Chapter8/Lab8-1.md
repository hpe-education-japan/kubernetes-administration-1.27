# 課題8.1: ConfigMap の作成

**概要**

アプリケーションの中には、コンテナファイルが短命であることが問題となるものもあります。ファイルは、コンテナを再起動する際に消えてしまいます。また、Pod 内のコンテナ間でファイルを共有する手段も必要になります。  

Volume はPod 内のコンテナがアクセスできるディレクトリです。クラウドプロバイダはPod のライフタイムより長く残るVolumeを提供しているため、AWS やGCE のVolume を事前にデータを格納した状態でPod に提供することや、1 つのPod から他のPod へVolume を移動することも可能です。Ceph も動的かつ永続的なVolume を実現するためのソリューションとして好評です。  

現在のDocker Volume とは異なり、Kubernetes のVolume は、Pod 内のコンテナではなくPod と同じライフタイムを持っています。また、同じPod 内で異なる種類のVolume を同時に利用することは可能ですが、Volume をネストする形でマウントすることはできません。それぞれのVolume に各自のマウントポイントが必要です。Volume はspec.volumes を用いて定義し、マウントポイントはspec.containers.volumeMounts パラメータを用いて定義します。Volume は現在24 種類あり、種類によっては他の制限がある可能性もあります。  
https://kubernetes.io/docs/concepts/storage/volumes/#types-of-volumes  

キーと値のペアの集合といえるConfigMap も利用します。このデータは、環境変数として、もしくは設定データとしてPod が読み込めるように提供できます。ConfigMap はbase64 でエンコードした配列ではありませんが、Secret と似ています。文字列として格納し、シリアル化した状態で読むことが可能です。  
<br>
<br>
ConfigMap にデータを取り込む方法は3 つあります: リテラル値から、ファイルから、そしてディレクトリから取り込むことが可能です。  

**1. データに色を含むConfigMap を作成しましょう。ConfigMap に取り込むためのファイルを作成します。まず、primary というディレクトリを作成し、4 つのファイルを作成します。次に、ホームディレクトリ内にあなたが好きな色を含むファイルを作成します。例では"blue" です。**

```
student@setX-cp:~$ mkdir primary
student@setX-cp:~$ echo c > primary/cyan
student@setX-cp:~$ echo m > primary/magenta
student@setX-cp:~$ echo y > primary/yellow
student@setX-cp:~$ echo k > primary/black
student@setX-cp:~$ echo "known as key" >> primary/black
student@setX-cp:~$ echo blue > favorite
```

**2. 作成したファイルとリテラル値を含むConfigMap を作成します。**
```
student@setX-cp:~$ kubectl create configmap colors --from-literal=text=black --from-file=./favorite --from-file=./primary/

configmap/colors created
```

**3. ConfigMap が保存されていることを確認します。まずyaml フォーマットで出力し、内容を確認します。-o json オプションを使ってjson フォーマットで出力することも可能です。**
```
student@setX-cp:~$ kubectl get configmap colors

NAME     DATA   AGE
colors   6      30s
```
```
student@setX-cp:~$ kubectl get configmap colors -o yaml

apiVersion: v1
data:
  black: |
    k
    known as key
  cyan: |
    c
  favorite: |
    blue
  magenta: |
    m
  text: black
  yellow: |
    y
kind: ConfigMap
< 省略>
```

**4. 次に、ConfigMap を利用するPod を作成しましょう。ここでは環境変数ilike にConfigMap のデータを設定しています。**
```
student@setX-cp:~$ vim simpleshell.yaml
```
![chap8-1](/uploads/636b15c471c45cc0dce6eefcc42f19f9/chap8-1.png)

[simpleshell.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/simpleshell-0.yaml)


**5. Pod を作成し、環境変数を確認しましょう。パラメータを確認後、Pod からexit し、Pod を削除します。**
```
student@setX-cp:~$ kubectl create -f simpleshell.yaml

pod/shell-demo created
```
```
student@setX-cp:~$ kubectl exec shell-demo -- /bin/bash -c 'echo $ilike'

blue
```
```
student@setX-cp:~$ kubectl delete pod shell-demo

pod "shell-demo" deleted
```

**6. ConfigMap のすべてのデータを環境変数として含むことも可能です。env: の段落をコメントアウトし、envFrom を追加します。前の設定と新しい設定を比較してみましょう。違いを理解するのに役立ちます。Pod を再作成し、すべての変数を確認してから、Pod を削除します。環境変数の出力にConfigMap に設定したすべてのデータが含まれているはずです。**
```
student@setX-cp:~$ vim simpleshell.yaml
```
![chap8-2](/uploads/f52a350dd4f7b5a5af409bd1e3e0b3b7/chap8-2.png)

[simpleshell.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/simpleshell-1.yaml)

```
student@setX-cp:~$ kubectl create -f simpleshell.yaml

pod/shell-demo created
```
```
student@setX-cp:~$ kubectl exec shell-demo -- /bin/bash -c 'env'

black=k
known as key

KUBERNETES_SERVICE_PORT_HTTPS=443
cyan=c
< 省略>
```
```
student@setX-cp:~$ kubectl delete pod shell-demo

pod "shell-demo" deleted
```

**7. ConfigMap をYAML ファイルから作成することも可能です。車を定義するパラメータを含むYAML ファイルを作成しましょう。**
```
student@setX-cp:~$ vim car-map.yaml
```
![chap8-3](/uploads/1fcfb61e58e507d693941346e3f793a0/chap8-3.png)

[car-map.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/car-map.yaml)


**8. ConfigMap を作成しましょう。それから設定を検証しましょう。**
```
student@setX-cp:~$ kubectl create -f car-map.yaml

configmap/fast-car created
```
```
student@setX-cp:~$ kubectl get configmap fast-car -o yaml
```

![chap8-4](/uploads/fb715df00ac6280dd1d9a84513083041/chap8-4.png)


**9. 次に、ConfigMap のデータをPod 内のコンテナにVolume としてマウントします。以前の環境設定をコメントアウトし、次の設定を追加します。containers: とvolumes: の行は、同じインデント幅です。**
```
student@setX-cp:~$ vim simpleshell.yaml
```
![chap8-5](/uploads/8dc3699ae18ed83fc0568d3cf372de86/chap8-5.png)

[simpleshell.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/simpleshell-2.yaml)


**10. Pod を再作成しましょう。Volume がマウントされていることと、Volume 内のあるファイルの内容を確認します。ファイルは改行を含んでいないため、次のプロンプトはShelby という出力と同じ行に表示される可能性があります。**
```
student@setX-cp:~$ kubectl create -f simpleshell.yaml

pod "shell-demo" created
```
```
student@setX-cp:~$ kubectl exec shell-demo -- /bin/bash -c 'df -ha |grep car'

/dev/xvda1      9.6G  3.2G  6.4G  34%   /etc/cars
```
```
student@setX-cp:~$ kubectl exec shell-demo -- /bin/bash -c 'cat /etc/cars/car.trim'

Shelby　# に続いてあなたのプロンプトが表示されます。
```

**11. 利用していたPod とConfigMap を削除しましょう。**
```
student@setX-cp:~$ kubectl delete pods shell-demo

pod "shell-demo" deleted
```
```
student@setX-cp:~$ kubectl delete configmap fast-car colors

configmap "fast-car" deleted  
configmap "colors" deleted
```
  

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/Lab8-2.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)

