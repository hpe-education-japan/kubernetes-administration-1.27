# 課題8.4: PVC 数と消費量の制限にResourceQuota を利用

柔軟なクラウドベースのストレージを利用する際には、ストレージ使用量の制限が必要となるでしょう。ResourceQuota オブジェクトを利用して、全体のストレージ使用量とPVC の数を制限しましょう。

**1. まずは、先ほどNFS を利用するために作成したDeployment と、PV とPVC を削除しましょう。**
```
student@setX-cp:~$ kubectl delete deploy nginx-nfs

deployment.apps "nginx-nfs" deleted
```
```
student@setX-cp:~$ kubectl delete pvc pvc-one

persistentvolumeclaim "pvc-one" deleted
```
```
student@setX-cp:~$ kubectl delete pv pvvol-1

persistentvolume "pvvol-1" deleted
```

**2. ResourceQuota 用のYAML ファイルを作成しましょう。ストレージの制限はPVC を10 個まで、全体の使用量を500Mi に設定します。**
```
student@setX-cp:~$ vim storage-quota.yaml
```
![chap8-9](/uploads/800a33441990e0ed802a4d779a72f6c0/chap8-9.png)

[storage-quota.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/storage-quota-0.yaml)


**3. small というnamespace を新規作成しましょう。新しいクォータを適用する前のnamespace の情報を確認しましょう。**

namespace は、正式名の「namespace」もしくは略称の「ns」で指定できます。  
```
student@setX-cp:~$ kubectl create namespace small

namespace/small created
```
```
student@setX-cp:~$ kubectl describe ns small

Name:         small
Labels:       <none>
Annotations:  <none>
Status:       Active

No resource quota.

No LimitRange resource.
```

**4. small namespace 内に新しいPV とPVC を作成しましょう。**
```
student@setX-cp:~$ kubectl -n small create -f PVol.yaml

persistentvolume/pvvol-1 created
```
```
student@setX-cp:~$ kubectl -n small create -f pvc.yaml

persistentvolumeclaim/pvc-one created
```

**5. small namespace にリソース制限を設定します。**
```
student@setX-cp:~$ kubectl -n small create -f storage-quota.yaml

resourcequota/storagequota created
```

**6. small namespace のクォータが存在することを確認します。先の手順で同じコマンドを実行したときの出力と比較しましょう。**
```
student@setX-cp:~$ kubectl describe ns small

Name:         small
Labels:       <none>
Annotations:  <none>
Status:       Active

Resource Quotas
 Name:                   storagequota
 Resource                Used   Hard
 --------                ---    ---
 persistentvolumeclaims  1      10
 requests.storage        200Mi  500Mi

No resource limits.
```

**7. nfs-pod.yamlファイルからnamespace の行を削除しましょう。11 行目辺りにあるはずです。これにより、コマンドラインで他のnamespace を指定することが可能になります。**
```
student@setX-cp:~$ vim nfs-pod.yaml
```

**8. 再度コンテナを作成しましょう。**
```
student@setX-cp:~$ kubectl -n small create -f nfs-pod.yaml

deployment.apps/nginx-nfs created
```

**9. Deployment を確認し、Pod が設定通り起動しているか確認しましょう。**
```
student@setX-cp:~$ kubectl -n small get deploy

NAME        READY   UP-TO-DATE   AVAILABLE   AGE
nginx-nfs   1/1     1            1           43s
```
```
student@setX-cp:~$ kubectl -n small describe deploy nginx-nfs

< 省略>
```

**10. Pod が準備完了な状態であるかを確認しましょう。**
```
student@setX-cp:~$ kubectl -n small get pod

NAME                         READY   STATUS    RESTARTS   AGE
nginx-nfs-2854978848-g3khf   1/1     Running   0          37s  
```

**11. Pod が起動し、NFS Volume がマウントされていることを確認しましょう。-n オプションの引数としてNamespace 名を指定すれば、Pod 名の指定にTab 補完が利用できます。**
```
student@setX-cp:~$ kubectl -n small describe pod nginx-nfs-2854978848-g3khf

Name:         nginx-nfs-2854978848-g3khf
Namespace:    small
< 省略>
    Mounts:
      /opt from nfs-vol (rw)
< 省略>
```

**12. small namespace のクォータとストレージ使用量を確認しましょう。**
```
student@setX-cp:~$ kubectl describe ns small

< 省略>  
Resource Quotas
 Name:                   storagequota
 Resource                Used   Hard
 --------                ---    ---
 persistentvolumeclaims  1      10
 requests.storage        200Mi  500Mi

No resource limits.
```

**13. ホスト上の/opt/sfwディレクトリ内に300M のファイルを作成し、ストレージ使用量を再度確認しましょう。NFS 共有ディレクトリ内のストレージ使用量は、namespace 内でのストレージ使用量に影響しないことに注目しましょう。**
```
student@setX-cp:~$ sudo dd if=/dev/zero of=/opt/sfw/bigfile bs=1M count=300

300+0 records in  
300+0 records out  
314572800 bytes (315 MB, 300 MiB) copied, 0.196794 s, 1.6 GB/s
```
```
student@setX-cp:~$ kubectl describe ns small  

< 省略>  
Resource Quotas
 Name:                   storagequota
 Resource                Used   Hard
 --------                ---    ---
 persistentvolumeclaims  1      10
 requests.storage        200Mi  500Mi
< 省略>
```
```
student@setX-cp:~$ du -h /opt/

301M     /opt/sfw  
41M      /opt/cni/bin  
41M      /opt/cni  
341M     /opt/
```

**14. 次に、Deployment がクォータ以上のストレージを要求した場合について説明します。まずは既存のDeployment を削除しましょう。**
```
student@setX-cp:~$ kubectl -n small get deploy

NAME        READY   UP-TO-DATE   AVAILABLE   AGE
nginx-nfs   1/1     1            1           11m
```
```
student@setX-cp:~$ kubectl -n small delete deploy nginx-nfs

deployment.apps "nginx-nfs" deleted 
```

**15. Pod が停止したら、namespace のストレージ使用量を再度確認しましょう。Pod が削除された時点では、ストレージの削除が行われなかったことがわかります。**
```
student@setX-cp:~$ kubectl describe ns small

< 省略>  
Resource Quotas
 Name:                   storagequota
 Resource                Used   Hard
 --------                ---    ---
 persistentvolumeclaims  1      10
 requests.storage        200Mi  500Mi
```

**16. PVC を削除し、PVC が利用していたPV を確認してみましょう。RECLAIM POLICY とSTATUS を確認しましょう。**
```
student@setX-cp:~$ kubectl -n small get pvc

NAME      STATUS   VOLUME    CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-one   Bound    pvvol-1   1Gi        RWX                           19m
```
```
student@setX-cp:~$ kubectl -n small delete pvc pvc-one

persistentvolumeclaim "pvc-one" deleted
```
```
student@setX-cp:~$ kubectl -n small get pv

NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS     CLAIM           STORAGECLASS   REASON   AGE
pvvol-1   1Gi        RWX            Retain           Released   small/pvc-one                           44m
```

**17. 動的プロビジョニングしたストレージはStorageClass のReclaimPolicy を利用します。ReclaimPolicy にはDelete、Retain、ストレージの種類によってはRecycle が設定できます。手動で作成したPV は、作成時に指定がなければ、デフォルトでRetain が設定されます。デフォルトのストレージポリシーはストレージを保持(Retain) し、データの復元を可能にします。ストレージポリシーを変更するために、まずPV 設定のYAML 出力を確認しましょう。**
```
student@setX-cp:~$ kubectl get pv/pvvol-1 -o yaml
```
![chap8-10](/uploads/b675574faad5d8fd1db9fc57556980f1/chap8-10.png)


**18. 現時点ではオブジェクトを削除し、再作成する必要があります。削除プラグインは現在開発中です。まず、Retain ポリシーを利用するVolume を再作成しましょう。**
```
student@setX-cp:~$ kubectl delete pv/pvvol-1

persistentvolume "pvvol-1" deleted
```
```
student@setX-cp:~$ grep Retain PVol.yaml

persistentVolumeReclaimPolicy: Retain
```
```
student@setX-cp:~$ kubectl create -f PVol.yaml

persistentvolume "pvvol-1" created
```

**19. kubectl patch を利用してReclaimPolicy をRetain からDelete に変更します。書式に関しては、先ほどのYAML 出力を参考にしましょう。**
```
student@setX-cp:~$ kubectl patch pv pvvol-1 -p '{"spec":{"persistentVolumeReclaimPolicy":"Delete"}}'

persistentvolume/pvvol-1 patched
```
```
student@setX-cp:~$ kubectl get pv/pvvol-1

NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
pvvol-1   1Gi        RWX            Delete           Available                                   2m
```

**20. 現時点のクォータ設定を確認しましょう。**
```
student@setX-cp:~$ kubectl describe ns small

....  
 requests.storage        0     500Mi
```

**21. PVC を再作成しましょう。Pod が起動していないにも関わらず、ストレージが使用されていることがわかります。**
```
student@setX-cp:~$ kubectl -n small create -f pvc.yaml

persistentvolumeclaim/pvc-one created
```
```
student@setX-cp:~$ kubectl describe ns small

....  
 requests.storage        200Mi  500Mi
```

**22. small namespace に設定されているクォータを削除しましょう。**
```
student@setX-cp:~$ kubectl -n small get resourcequota

NAME           AGE     REQUEST                                                       LIMIT
storagequota   8m11s   persistentvolumeclaims: 1/10, requests.storage: 200Mi/500Mi
```
```
student@setX-cp:~$ kubectl -n small delete resourcequota storagequota

resourcequota "storagequota" deleted
```

**23. storagequota.yaml ファイルを編集し、容量を100Mi に減らしましょう。**
```
student@setX-cp:~$ vim storage-quota.yaml
```
![chap8-11](/uploads/1fb36988ee839b7c9720586a32afabd3/chap8-11.png)

[storage-quota.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/storage-quota-1.yaml)


**24. 新しいストレージクォータを作成し、確認しましょう。ハードリミットを既に超えていることがわかるはずです。**
```
student@setX-cp:~$ kubectl -n small create -f storage-quota.yaml

resourcequota/storagequota created
```
```
student@setX-cp:~$ kubectl describe ns small

....
 persistentvolumeclaims  1      10
 requests.storage        200Mi  100Mi

No resource limits. 
```

**25. Deployment を再作成しましょう。それから詳細を確認してみましょう。エラーは表示されていません。**
```
student@setX-cp:~$ kubectl -n small create -f nfs-pod.yaml

deployment.apps/nginx-nfs created
```
```
student@setX-cp:~$ kubectl -n small describe deploy/nginx-nfs

Name:                   nginx-nfs
Namespace:              small
< 省略>  
```

**26. Pod が実際に起動しているかどうかを確認しましょう。**
```
student@setX-cp:~$ kubectl -n small get po

NAME                         READY   STATUS    RESTARTS   AGE
nginx-nfs-2854978848-vb6bh   1/1     Running   0          58s  
```

**27. ハードクォータを設定したにも関わらず、それを超える容量のPod をデプロイしたので、ストレージの再要求が行われるかどうかを検証しましょう。Deployment とPVC を削除してみましょう。**
```
student@setX-cp:~$ kubectl -n small delete deploy nginx-nfs

deployment.apps "nginx-nfs" deleted
```
```
student@setX-cp:~$ kubectl -n small delete pvc/pvc-one

persistentvolumeclaim "pvc-one" deleted
```

**28. PV がまだ存在しているかを確認しましょう。削除に失敗しています。kubectl -n small describe pv で詳しく見てみると、原因はNFS のdeleter volume plugin がないためであることがわかります。他のストレージプロトコルにはプラグインが存在します。**
```
student@setX-cp:~$ kubectl -n small get pv

NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM           STORAGECLASS   REASON   AGE
pvvol-1   1Gi        RWX            Delete           Failed   small/pvc-one                           20m
```

**29. PV を削除します。そして、Deployment、PVC、PV がすべて削除されていることをkubectl get で確認しましょう。**
```
student@setX-cp:~$ kubectl delete pv/pvvol-1

persistentvolume "pvvol-1" deleted
```

**30. ここからは、persistentVolumeReclaimPolicy: をRecycle に変更して実施してみましょう。PV のYAML ファイルを編集します。**
```
student@setX-cp:~$ vim PVol.yaml
```
![chap8-12](/uploads/0914a5c924aecdc4398b4bfe1a4b1356/chap8-12.png)

[PVol.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/PVol-1.yaml)


**31. namespace にLimitRange を追加します。以前利用したLimitRange を利用します。**
```
student@setX-cp:~$ kubectl -n small create -f low-resource-range.yaml

limitrange/low-resource-range created
```

**32. namespace の設定を確認しましょう。クォータとリソースリミットの両方が表示されるはずです。**
```
student@setX-cp:~$ kubectl describe ns small

< 省略>  
Resource Limits
 Type       Resource  Min  Max  Default Request  Default Limit  ...
 ----       --------  ---  ---  ---------------  -------------  ...
 Container  cpu       -    -    500m             1              -
 Container  memory    -    -    100Mi            500Mi          -
```

**33. PV を再作成し、Reclaim Policy がRecycle であることを確認します。**
```
student@setX-cp:~$ kubectl -n small create -f PVol.yaml

persistentvolume/pvvol-1 created
```
```
student@setX-cp:~$ kubectl get pv

NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      ...
pvvol-1   1Gi        RWX            Recycle          Available   ...
```

**34. PV とPVC の再作成を試みましょう。クォータは、リソースリミットもある場合のみ有効です。**
```
student@setX-cp:~$ kubectl -n small create -f pvc.yaml

Error from server (Forbidden): error when creating "pvc.yaml": persistentvolumeclaims "pvc-one" is forbidden: exceeded quota: storagequota, requested: requests.storage=200Mi, used: requests.storage=0, limited: requests.storage=100Mi
```

**35. resourcequota を編集し、requests.storage を500Mi に増やしましょう。**
```
student@setX-cp:~$ kubectl -n small edit resourcequota
```
![chap8-13](/uploads/534f686ded953021cd94462ccd87d06a/chap8-13.png)


**36. PVC を再作成してみましょう。今回は成功するはずです。次に、Deployment を再作成します。**
```
student@setX-cp:~$ kubectl -n small create -f pvc.yaml

persistentvolumeclaim/pvc-one created
```
```
student@setX-cp:~$ kubectl -n small create -f nfs-pod.yaml

deployment.apps/nginx-nfs created
```

**37. namespace の設定を確認しましょう。**
```
student@setX-cp:~$ kubectl describe ns small

< 省略>
```

**38. Deployment を削除しましょう。それからPV とPVC の状態を確認します。**
```
student@setX-cp:~$ kubectl -n small delete deploy nginx-nfs

deployment.apps "nginx-nfs" deleted
```
```
student@setX-cp:~$ kubectl -n small get pvc

NAME      STATUS   VOLUME    CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-one   Bound    pvvol-1   1Gi        RWX                           7m
```
```
student@setX-cp:~$ kubectl -n small get pv

NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM           ...          
pvvol-1   1Gi        RWX            Recycle          Bound    small/pvc-one   ...
```

**39. PVC を削除し、PV の状態を確認しましょう。Available と表示されているはずです。**
```
student@setX-cp:~$ kubectl -n small delete pvc pvc-one

persistentvolumeclaim "pvc-one" deleted
```
```
student@setX-cp:~$ kubectl -n small get pv

NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORA...
pvvol-1   1Gi        RWX            Recycle          Available                ...
```

**40. PV を削除し、この演習で作成したリソースをすべて削除したことを確認しましょう。**
```
student@setX-cp:~$ kubectl delete pv pvvol-1

persistentvolume "pvvol-1" deleted
```
  

以上

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
