# 課題8.2: NFS によるPersistent Volume の作成

まずNFS サーバをデプロイします。NFS サーバの検証後、Pod がClaim(要求) するNFS のPersistent Volume を作成します。  

**1. コントロールプレーンノードにNFS サーバをインストールしましょう。**
```
student@setX-cp:~$ sudo apt-get update && sudo apt-get install -y nfs-kernel-server

< 省略>
```

**2. NFS で共有するためのディレクトリを作成し、ファイルを作成します。/tmp/ディレクトリと同じパーミッションを設定します。**
```
student@setX-cp:~$ sudo mkdir /opt/sfw
student@setX-cp:~$ sudo chmod 1777 /opt/sfw/
student@setX-cp:~$ sudo bash -c 'echo software > /opt/sfw/hello.txt'
```

**3. NFS サーバの設定ファイルを編集し、新規に作成したディレクトリを共有するようにしましょう。ここでは、どこからでもディレクトリを共有できるようにします。後からsnoop などのネットワーク監視ツールを利用してリクエストを確認し、このディレクトリへのアクセスを限定するこもが可能です。**
```
student@setX-cp:~$ sudo vim /etc/exports

以下を追加

/opt/sfw/ *(rw,sync,no_root_squash,subtree_check)
```  

**4. /etc/exportsの設定の変更を反映します。**
```
student@setX-cp:~$ sudo exportfs -ra
```

**5. ワーカノードから共有ディレクトリをマウントできることを確認します。**

**ワーカーノードで実施**
```
student@setX-worker:~$ sudo apt-get -y install nfs-common

< 省略>
```
```
student@setX-worker:~$ showmount -e k8scp

Export list for k8scp:  
/opt/sfw *
```
```
student@setX-worker:~$ sudo mount k8scp:/opt/sfw /mnt
student@setX-worker:~$ ls -l /mnt

total 4  
-rw-r--r-- 1 root root 9 Sep 28 17:55 hello.txt 
``` 

**6. コントロールプレーンノードでPersistentVolume を作成するためのYAML ファイルを作成しましょう。マスタノードのホスト名と先ほど作成したディレクトリを利用します。YAML ファイルの構文確認しか行われないので、間違った名前やディレクトリを指定してもエラーは発生しません。ただし、設定に間違いのあるPersistentVolume を利用しようとするPod は起動に失敗します。また、現時点でaccessModes は実際にはアクセス制御を行わず、Label として利用されることに注意しましょう。**

**コントロールプレーンノードで実施**

```
student@setX-cp:~$ vim PVol.yaml
```
![chap8-6](/uploads/8cd950f5be50a11755f5105b4e5e62c3/chap8-6.png)

[PVol.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/labfiles/PVol-0.yaml)


**7. PersistentVolume を作成し、作成できたことを確認しましょう。**
```
student@setX-cp:~$ kubectl create -f PVol.yaml

persistentvolume/pvvol-1 created
```
```
student@setX-cp:~$ kubectl get pv

NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
pvvol-1   1Gi        RWX            Retain           Available                                   4s
```

以上  

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/Lab8-3.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
