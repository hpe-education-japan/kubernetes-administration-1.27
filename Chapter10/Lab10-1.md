# 課題10.1: Helm とChart の利用

**概要**

Helm は複雑な設定のデプロイを容易にします。複数のアプリケーションをワンステップでデプロイしたい場合に有用です。Chart、つまりテンプレートファイルを利用して必須コンポーネントとその関係性を定義します。Tiller がAPI を利用してオブジェクトを作成します。つまり、オーケストレーションのためのオーケストレーションです。  
  
Helm のインストール方法はいくつかあります。最新のバージョンはソースコードからビルドする必要があるかもしれません。ここでは最近の安定版をダウンロードします。インストール後、クラスタにMariaDB を設定するChart をデプロイします。  

**Helm のインストール**

**1. コントロールプレーンノードでwget を実行してtar.gz ファイルをダウンロードします。さまざまなバージョンがこちらで確認できます。最新バージョンをインストールした方が良いでしょう。:**

https://github.com/helm/helm/releases/  
```
student@setX-cp:~$ wget wget https://get.helm.sh/helm-v3.12.0-linux-amd64.tar.gz

< 省略 >
helm-v3.12.0-linux-a 100%[===================>] 13.35M --.-KB/s in 0.1s

2023-06-01 03:18:50 (52.0 MB/s) - ‘helm-v3.12.0-linux-amd64.tar.gz’ saved [14168950/14168950]
```

**2. ファイルを解凍・展開します。**
```
student@setX-cp:~$  tar -xvf helm-v3.12.0-linux-amd64.tar.gz

linux-amd64/
linux-amd64/helm
linux-amd64/README.md
linux-amd64/LICENSE
```

**3. helm バイナリをシェルの検索パス経由で利用できるように/usr/local/bin/ディレクトリにコピーします。**
```
student@setX-cp:~$  sudo cp linux-amd64/helm /usr/local/bin/helm
```

**4. Chart は、アプリケーションをデプロイするためのファイルの集合です。ベンダーから提供されているhttps://github.com/
kubernetes/charts/tree/master/stable に良いスタートレポがあります。また、独自のものを作成してもよいでしょう。利用可能な安定版データベースの現在のChart を、Helm Hub やMonocular インスタンスから検索します。レポは頻繁に変わるため、次の出力はあなたの見るものと少し違うかもしれません。**
```
student@setX-cp:~$ helm search hub database

URL                                                 CHART VERSION  APP VERSION  DESCRIPTION
https://artifacthub.io/packages/helm/drycc/data...  1.0.2          A            PostgreSQL database used by Drycc Workflow.
https://artifacthub.io/packages/helm/drycc-cana...  1.0.0          A            PostgreSQL database used by Drycc Workflow.
https://artifacthub.io/packages/helm/camptocamp...  0.0.6          1.0          Expose services and secret to access postgres d...
https://artifacthub.io/packages/helm/cnieg/h2-d...  1.0.3          1.4.199      A helm chart to deploy h2-database
< 省略>  
```

**5. echo プログラムがあるealenn といった、様々なベンダーが提供しているリポジトリを追加できます。これらのリポジトリは、ほと
んどの場合artifacthub.ioを検索することで見つかります。**
```
student@setX-cp:~$ helm repo add ealenn https://ealenn.github.io/charts

"ealenn" has been added to your repositories
```
```
student@setX-cp:~$ helm repo update

Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "ealenn" chart repository
Update Complete. Happy Helming!
```

**6.  tester をインストールします。**  

--debug オプションで詳細なログを出力します。ソフトウェアへのアクセス方法も出力されます。  

```
student@setX-cp:~$ helm upgrade -i tester ealenn/echo-server --debug

history.go:56: [debug] getting history for release tester
Release "tester" does not exist. Installing it now.
install.go:173: [debug] Original chart version: ""
install.go:190: [debug] CHART PATH: /home/student/.cache/helm/repository/echo-server-0.3.0.tgz

client.go:122: [debug] creating 4 resource(s)
NAME: tester
< 省略>
```

**7. 新しく作成したtester-echo-server Pod が起動していることを確かめます。起動していない場合は問題を修正します。**
```
student@setX-cp:~$ kubectl get pods
```

**8. 新しく作成した Service を探します。curl によって Cluster IP にリクエストを送ります。様々な情報が取得できるはずです。**
```
student@setX-cp:~$ kubectl get svc

NAME               TYPE      CLUSTER-IP   EXTERNAL-IP PORT(S)  AGE
kubernetes         ClusterIP 10.96.0.1    <none>      443/TCP  26h
tester-echo-server ClusterIP 10.98.252.11 <none>      80/TCP   11m
```
```
student@setX-cp:~$ curl 10.98.252.11

{"host":{"hostname":"10.98.252.11","ip":"::ffff:192.168.74.128","ips":[]},"http":{"method":"GET","baseUrl":"","originalUrl":"/","protocol":
"http"},"request":{"params":{"0":"/"},"query":{},"cookies":{},"body":{},"headers":{"host":"10.98.252.11","user-agent":"curl/7.58.0","accept":
"*/*"}},"environment":{"PATH":"/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin","TERM":"xterm","HOSTNAME":"tester-echo-server7 786768d9f4-4zsz9","ENABLE__HOST":"true","ENABLE__HTTP":"true","ENABLE__
< 省略>
```

**9. デプロイされたChart の履歴を確認します。-a オプションを利用すると、deleted や failed を含むすべてのChart が表示されます。**
```
student@setX-cp:~$ helm list

NAME     NAMESPACE REVISION UPDATED                                 STATUS   CHART             APP VERSION
tester   default   1        2021-06-11 07:31:56.151628311 +0000 UTC deployed echo-server-0.5.0 0.6.0
```

**10. tester Chart を削除し、削除されたことを確認します。**
```
student@setX-cp:~$ helm uninstall tester

release "tester" uninstalled
```
```
student@setX-cp:~$ helm list

NAME NAMESPACE REVISION UPDATED STATUS CHART APP VERSION
```

**11. デプロイ時にダウンロードされた Chart を探します。ユーザのホームディレクトリ以下に tgz ファイルがあるはずです。echo のバ
ージョンは少し異なるかもしれません。**
```
student@setX-cp:~$ find $HOME -name *echo*

/home/student/.cache/helm/repository/echo-server-0.5.0.tgz
```

**12. ディレクトリを移動し、tgz ファイルを展開します。内容を確認しましょう。**
```
student@setX-cp:~$ cd $HOME/.cache/helm/repository ; tar -xvf echo-server-*

echo-server/Chart.yaml
echo-server/values.yaml
echo-server/templates/_helpers.tpl
echo-server/templates/configmap.yaml
echo-server/templates/deployment.yaml
< 省略>
```

**13. values.yamlファイルを見て、設定可能値を確認します。**
```
student@setX-cp:~/.cache/helm/repository$ cat echo-server/values.yaml

< 省略>
```

**14. values.yamlファイルをインストール前にダウンロードし、内容を確認し編集することも可能です。別のリポジトリを追加し、Bitnami Apache Chart をダウンロードします。**
```
student@setX-cp:~/.cache/helm/repository$ cd ~
student@setX-cp:~$ helm repo add bitnami https://charts.bitnami.com/bitnami
student@setX-cp:~$ helm fetch bitnami/apache --untar
student@setX-cp:~$ cd apache/
```

**15. Chart を見てみましょう。先に確認したものとは違うはずです。:values.yaml: の全体を見てみましょう。**
```
student@setX-cp:~$ ls

Chart.lock Chart.yaml README.md charts ci files templates values.schema.json values.yaml
```
```
student@setX-cp:~$ less values.yaml

## Global Docker image parameters
## Please, note that this will override the image parameters, including dependencies, configured....
## Current available global Docker image parameters: imageRegistry and imagepullSecrets
##
# global:
# imageRegistry: myRegistryName
# imagePullSecrets:
# - myRegistryKeySecretName
< 省略>
```

**16. values.yamlファイルを使って Chart をインストールします。出力を見て、Pod が起動していることを確認して下さい。**
```
student@setX-cp:~$ helm install anotherweb .

NAME: anotherweb
LAST DEPLOYED: Fri Jun 11 08:11:10 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
< 省略>
```

**17. 新しく作成した Service をテストします。It works! を表示する HTML レスポンスが取得できるはずです。正しく表示されるまで少し時間がかかるかもしれません。**

**18. helm を利用してインストールしたもの全てを削除します。コマンドを覚えていない場合、この章の以前の手順を参照して下さい。helm は他の演習でまた利用します。**


以上

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
