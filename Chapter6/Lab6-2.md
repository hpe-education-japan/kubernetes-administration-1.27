# 課題6.2: プロキシを利用する

API を利用するもう1 つの方法はプロキシを利用する方法です。プロキシはノードから起動することも、Pod 内からサイドカーを利用して起動することもできます。次の手順では、ループバックアドレスで待ち受けするプロキシを起動します。curl を利用してAPI サーバにアクセスします。curl リクエストがプロキシ経由で成功し、クラスタ外からは成功しない場合、原因はAPI 処理部分ではなく、認証と認可の段階にあるかもしれません。  

**1. まず、プロキシを起動しましょう。デフォルトでフォアグラウンドで起動するようになっています。利用できるオプションは複数あります。まずは、help の出力内容を確認します。**
```
student@setX-cp:~$ kubectl proxy -h

Creates a proxy server or application-level gateway between localhost and the Kubernetes API Server. It also allows
serving static content over specified HTTP path. All incoming data enters through one port and gets forwarded to the
remote kubernetes API Server port, except for the path matching the static content path.

Examples:
  # To proxy all of the kubernetes api and nothing else, use:

  $ kubectl proxy --api-prefix=/
< 省略>
```

**2. API のプレフィックスを設定して、バックグラウンドでプロキシを起動します。Enter を押さないと、プロンプトが表示されない可能性があります。以下の例では、プロセスID である225000 をメモしておきます。プロセスを終了させるために使います。**
```
student@setX-cp:~$ kubectl proxy --api-prefix=/ &

[1] 22500  
Starting to serve on 127.0.0.1:8001 
```

**3. プロキシコマンド実行時に表示されたIP アドレスとポート番号を指定して、curl コマンドを実行します。出力内容はプロキシを通さない場合と同じになるはずです。ただし、見た目は多少異なっているかもしれません。**
```
student@setX-cp:~$ curl http://127.0.0.1:8001/api/

< 省略>
```

**4. namespace の一覧を表示させるためのAPI 呼び出しを行います。先ほどはパーミッションの関係でコマンドが失敗しましたが、今回はプロキシが代わりにリクエストをしているため成功するはずです。**
```
student@setX-cp:~$ curl http://127.0.0.1:8001/api/v1/namespaces

{
  "kind": "NamespaceList",
  "apiVersion": "v1",
  "metadata": {
    "selfLink": "/api/v1/namespaces",
    "resourceVersion": "86902"
< 省略>
```

**5. これ以上必要ないのでプロキシサービスを停止してください。前のステップで使用したプロセスID を使用します。あなたのプロ
セスID は異なるはずです。**
```
student@setX-cp:~$ kill 22500
```

以上  

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/Lab6-3.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)

