# 課題6.1: RESTful なAPI へのアクセス

**概要**

引き続きクラスタのコントロールプレーンへアクセスする方法を見ていきます。Security の章では認証方法が複数あることについて説明しますが、その1 つがBearer token を利用する方法です。ここではBearer token を利用して、アプリケーションレベルでKubernetes API にアクセスするためにローカルのプロキシサーバをデプロイします。  
<br>
curl コマンドを利用してセキュアではない方法でクラスタにAPI リクエストを送ります。IP アドレスとポート番号とトークンがわかれば、RESTful な方法でクラスタのデータを取得できます。デフォルトではほとんどの情報へのアクセスが制限されますが、認証ポリシーを変更することでアクセスできる範囲を増やすことができます。  

**0. directoryを移動していたらホームに戻っておいてください。**
```
student@setX-cp:~$ cd ~
```

**1. まずはAPI サーバのレプリカを起動しているノードのIP アドレスとポート番号を知る必要があります。一般的にはコントロールプレーンノードがレプリカを1 つ実行しているでしょう。kubectl config view を利用してクラスタ設定を確認し、サーバのIP アドレスとポート番号を確認しましょう。これにより、IP とポートの両方を取得することができます。**
```
student@setX-cp:~$ kubectl config view

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://k8scp:6443
  name: kubernetes
< 省略>
```

**2. 最近のリリースでは、Kubernetes によるトークンシークレットの作成は、もはや自動ではありません。そのため、ユーザが望むとおりに設計する必要があります。以下に示すコマンドを利用して、トークンを作成します。**
```
student@setX-cp:~$ export token=$(kubectl create token default)
```
**3. クラスタから基本的な API の情報を取得できるかを試します。サーバ名、ポート番号、トークンを渡します。また、-k オプションを利用することで証明書を利用する必要がなくなります。**
```
student@setX-cp:~$ curl https://k8scp:6443/apis --header "Authorization: Bearer $token" -k

{
  "kind": "APIGroupList",
  "apiVersion": "v1",
  "groups": [
    {
      "name": "apiregistration.k8s.io",
      "versions": [
        {
          "groupVersion": "apiregistration.k8s.io/v1",
          "version": "v1"
< 省略>

```

**4. 同じコマンドで、今度は API v1 の情報を表示します。ただし、パスが/apis から/api/v1 に変わっていることに注意しましょう。**
```
student@setX-cp:~$ curl https://k8scp:6443/api/v1 --header "Authorization: Bearer $token" -k

< 省略>
```

**5. 次にnamespace の一覧を取得します。エラーとなるはずです。クラスタ内のすべてのnamespace を見るためのRBAC 権限を持っていないsystem:serviceaccount:default ユーザとして閲覧しようとしているからです。**
```
student@setX-cp:~$ curl https://k8scp:6443/api/v1/namespaces --header "Authorization: Bearer $token" -k

< 省略>  
"message": "namespaces is forbidden: User \"system:serviceaccount:default...  
< 省略>
```

**6. Pod に含まれている証明書を活用してAPI を利用することができます。**

証明書は/var/run/secrets/kubernetes.io/serviceaccount/ というディレクトリ以下に自動的に配置されていて、Podから利用できるようになっています。シンプルなPod をデプロイしてリソースを見ていきます。tokenファイルを開くと、$token 変数に設定した値と同じであることが確認できます。busybox コンテナに対して-i で標準入力を開き、-t でターミナルを割り当てます。ターミナルを終了してもコンテナは再起動せず、Pod は「Completed」状態となります。  
```
student@setX-cp:~$ kubectl run -i -t busybox --image=busybox --restart=Never
```

**コンテナ内**
```
# ls /var/run/secrets/kubernetes.io/serviceaccount/

ca.crt namespace token
```
```
# exit
```

**コンテナ内ここまで**

**7. 最後にbusybox Pod を削除します。**
```
student@setX-cp:~$ kubectl delete pod busybox

pod "busybox" deleted
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/Lab6-2.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
