# 課題6.3: Job の利用

多くのAPI オブジェクトはデプロイすると起動し続けますが、特定の回数だけ実行したい場合にはJob、定期的に実行したい場合にはCronJob を利用することができます。

**[Job の作成]**

**1. 3 秒sleep してから停止するコンテナを起動するJob を作成します。**
```
student@setX-cp:~$ vim job.yaml
```
![chap6-1](/uploads/cad7db1566ace19631b1205579d1e7e8/chap6-1.png)

[job.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/labfiles/job-0.yaml)


**2. Job を作成し、詳細を確認します。この例では3 秒以内にJob を確認し、完了後にもう一度確認します。入力のスピードによって異なる出力が表示される場合があります。**
```
student@setX-cp:~$ kubectl create -f job.yaml

job.batch/sleepy created
```
```
student@setX-cp:~$ kubectl get job

NAME     COMPLETIONS   DURATION   AGE
sleepy   0/1           3s         3s
```
```
student@setX-cp:~$ kubectl describe jobs.batch sleepy

Name:           sleepy
Namespace:      default
Selector:       controller-uid=24c91245-d0fb-11e8-947a-42010a800002
Labels:         controller-uid=24c91245-d0fb-11e8-947a-42010a800002
                job-name=sleepy
Annotations:    <none>
Parallelism:    1
Completions:    1
Start Time:     Tue, 16 Oct 2018 04:22:50 +0000
Completed At:   Tue, 16 Oct 2018 04:22:55 +0000
Duration:       5s
Pods Statuses:  0 Running / 1 Succeeded / 0 Failed
< 省略>
```
```
student@setX-cp:~$ kubectl get job

NAME     COMPLETIONS   DURATION   AGE
sleepy   1/1           5s         17s
```

**3. Job の設定情報を確認しましょう。Job の実行方法を変更する3 つのパラメータがあり、これらは-o yaml オプションをつけると確認できます。backoffLimit、completions、parallelism の3 つです。次はこれらのパラメータを追加していきます。**
```
student@setX-cp:~$ kubectl get jobs.batch sleepy -o yaml

< 省略>
  uid: c2c3a80d-d0fc-11e8-947a-42010a800002
spec:
  backoffLimit: 6
  completions: 1
  parallelism: 1
  selector:
    matchLabels:
< 省略>
```

**4. Job はCompleted の状態でAGE が増え続けているでしょう。Job を削除します。**
```
student@setX-cp:~$ kubectl delete jobs.batch sleepy

job.batch "sleepy" deleted
```

**5. YAML を編集します。completions: パラメータを追加し、5 に設定します。**
```
student@setX-cp:~$ vim job.yaml
```

![chap6-2](/uploads/a46c6819dd2f4e9b45f53701c6c67eb1/chap6-2.png)

[job.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/labfiles/job-1.yaml)


**6. Job を再作成し、確認しましょう。COMPLETIONS が0/5 から始まることがわかるでしょう。**
```
student@setX-cp:~$ kubectl create -f job.yaml

job.batch/sleepy created
```
```
student@setX-cp:~$ kubectl get jobs.batch

NAME     COMPLETIONS   DURATION   AGE
sleepy   0/5           5s         5s
```

**7. 起動中のPod を確認します。確認したタイミング次第で、出力内容は異なるかもしれません。**
```
student@setX-cp:~$ kubectl get pods

NAME           READY   STATUS      RESTARTS   AGE
sleepy-z5tnh   0/1     Completed   0          8s  
sleepy-zd692   1/1     Running     0          3s  
< 省略>
```

**8. しばらく待ってから再度確認をすると、すべてのJob が完了しているはずです。確認ができたら、Job を削除します。**
```
student@setX-cp:~$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
sleepy   5/5           26s        10m
```
```
student@setX-cp:~$ kubectl delete jobs.batch sleepy

job.batch "sleepy" deleted
```

**9. 再度YAML を編集しましょう。今回はparallelism: パラメータを追加します。Pod を一度に2 つずつデプロイするように2 に設定します。**
```
student@setX-cp:~$ vim job.yaml
```
![chap6-3](/uploads/e948bf0ae7753054d2d633b7940f91ef/chap6-3.png)

[job.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/labfiles/job-2.yaml)


**10. Job を再作成します。Pod が合計5 つになるまで、2 つずつデプロイされるはずです。**
```
student@setX-cp:~$ kubectl create -f job.yaml

job.batch/sleepy created
```
```
student@setX-cp:~$ kubectl get pods

NAME           READY   STATUS              RESTARTS   AGE
sleepy-8xwpc   1/1     Running             0          5s  
sleepy-xjqnf   1/1     Running             0          5s  
< 省略>
```
```
student@setX-cp:~$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
sleepy   3/5           11s        11s
```

**11. 特定の秒数が経過した時点でJob を停止するパラメータを追加しましょう。activeDeadlineSeconds: を15 に設定します。起動して15 秒経ったら、Job と関連するすべてのPod が停止します。また、sleep コマンドの引数を5 に増やし、15 秒のうちに実行できるPod の数を減らしてみます（3 秒ならPod は5 回まで、5 秒ならPod は3 回まで実行できるはずです）。**
```
student@setX-cp:~$ vim job.yaml
```
![chap6-4](/uploads/ae8a138ce1addf033703943555b0d8ad/chap6-4.png)

[job.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/labfiles/job-3.yaml)


**12. Job を削除し、再作成しましょう。15 秒経過したら、通常は3/5 以上（環境で変わります 2/5かもしれません）COMPLETIONS が増えないまま、AGE のみが増え続けます。**
```
student@setX-cp:~$ kubectl delete jobs.batch sleepy

job.batch "sleepy" deleted
```
```
student@setX-cp:~$ kubectl create -f job.yaml

job.batch/sleepy created
```
```
student@setX-cp:~$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
sleepy   1/5           6s        6s
```
```
student@setX-cp:~$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
sleepy   3/5           16s        16s
```

**13. オブジェクトのYAML 出力のStatus 部分のmessage: の行を確認しましょう。**
```
student@setX-cp:~$ kubectl get job sleepy -o yaml

< 省略>
status:
  conditions:
  - lastProbeTime: 2018-10-16T05:45:14Z
    lastTransitionTime: 2018-10-16T05:45:14Z
    message: Job was active longer than specified deadline
    reason: DeadlineExceeded
    status: "True"
    type: Failed
  failed: 2
  startTime: 2018-10-16T05:44:59Z
  succeeded: 3

```

**14. Job を削除します。**
```
student@setX-cp:~$ kubectl delete jobs.batch sleepy

job.batch "sleepy" deleted
```
<br>
<br>

**[CronJob の作成]**

CronJob は、指定した時間にバッチJob を実行するための監視ループを作成します。先ほど作成したJob ファイルを利用します。  

**1. Job ファイルを新しいファイルにコピーしましょう。**
```
student@setX-cp:~$ cp job.yaml cronjob.yaml
```

**2. 次のコメントを参考に、ファイルを編集しましょう。先ほど追加した3 つのパラメータの削除や、他の行のインデントを増やす作業が必要です。
(注意：テキストではCronJobのApiVersionがbatch/v1beta1と記載されていますが、Kubernetes V1.25からbatch/v1に変更になりました。cronjob.yamlリンクを参照して下さい。)**
```
student@setX-cp:~$ vim cronjob.yaml
```
![chap6-5](/uploads/e949afd7c3808558ffc4e18c4f4857fd/chap6-5.png)

[cronjob.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/labfiles/cronjob.yaml)


**3. 新規CronJob を作成してから、Job の一覧を取得してみましょう。CronJob が実行され、新規のバッチJob が生成されるまで2分程かかります。**
```
student@setX-cp:~$ kubectl create -f cronjob.yaml

cronjob.batch/sleepy created
``` 

```
student@setX-cp:~$ kubectl get cronjobs.batch

NAME     SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
sleepy   */2 * * * *   False     0        <none>          8s
```
```
student@setX-cp:~$ kubectl get jobs.batch

No resources found.
```

**4. 2 分後には、Job が起動しているはずです。**
```
student@setX-cp:~$ kubectl get cronjobs.batch

NAME     SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
sleepy   */2 * * * *   False     0        21s             2m1s  
```
```
student@setX-cp:~$ kubectl get jobs.batch

NAME                COMPLETIONS   DURATION   AGE
sleepy-1539722040   1/1           5s         18s
```
```
student@setX-cp:~$ kubectl get jobs.batch

NAME                COMPLETIONS   DURATION   AGE
sleepy-1539722040   1/1           5s         5m17s  
sleepy-1539722160   1/1           6s         3m17s  
sleepy-1539722280   1/1           6s         77s  
```

**5. Job が10 秒以上起動したままの場合、停止するようにします。sleep コマンドの引数を30 秒に変更してから、activeDeadlineSeconds:の行をコンテナに追加します。**

```
student@setX-cp:~$ vim cronjob.yaml
```

![chap6-7](/uploads/0a1b3715255bb178532c9e3ba12bc615/chap6-7.png)

[cronjob.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/labfiles/cron-job-1.yaml)


**6. CronJob を削除し、再作成します。バッチJob が作成され、タイマーによって停止するまで数分かかる可能性があります。**
```
student@setX-cp:~$ kubectl delete cronjobs.batch sleepy

cronjob.batch "sleepy" deleted
```
```
student@setX-cp:~$ kubectl create -f cronjob.yaml

cronjob.batch/sleepy created
```
```
student@setX-cp:~$ kubectl get jobs

NAME                COMPLETIONS   DURATION   AGE
sleepy-1539723240   0/1           61s        61s
```
COMPLETIONS は 0/1 です。  
```
student@setX-cp:~$ kubectl get cronjobs.batch

NAME     SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
sleepy   */2 * * * *   False     1        72s             94s
```
ACTIVEが1のままであることに注目して下さい。  
```
student@setX-cp:~$ kubectl get jobs

NAME                COMPLETIONS   DURATION   AGE
sleepy-1539723240   0/1           75s        75s  
```
```
student@setX-cp:~$ kubectl get jobs

NAME                COMPLETIONS   DURATION   AGE
sleepy-1539723240   0/1           2m19s      2m19s  
sleepy-1539723360   0/1           19s        19s  
```
```
student@setX-cp:~$ kubectl get cronjobs.batch

NAME     SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
sleepy   */2 * * * *   False     2        31s             2m53s  
```
ACTIVEが2である事に注目して下さい。  

**7. 最後にCronJob を削除します。**
```
student@setX-cp:~$ kubectl delete cronjobs.batch sleepy

cronjob.batch "sleepy" deleted
```


以上

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
