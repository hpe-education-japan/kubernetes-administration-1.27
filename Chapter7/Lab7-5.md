# 課題7.5: ローリングアップデートとロールバック

マイクロサービスの利点の 1 つは、クライアントリクエストに応答し続けながらもコンテナを置き換えたりアップグレードできることです。前のコンテナを削除したときにコンテナをアップグレードする OnDelete 設定を利用し、その後に、ローリングアップデートを即時開始する RollingUpdate 機能も利用します。  


**nginx のバージョン**

nginx ソフトウェアは、Kubernetes のアップデートとは異なるタイムラインに則ってアップデートされます。演習で古いバージョンを使用している場合、新しいデフォルトバージョンを利用してください。nginx のバージョンは次のコマンドで閲覧できます：  
```
student@setX-cp:~$ sudo crictl image ls | grep nginx
```

**1. まず、前述のセクションで作成したDaemonSet の現在のupdateStrategy の設定を確認します。**
```
student@setX-cp:~$ kubectl get ds ds-one -o yaml | grep -A 4 Strategy

  updateStrategy:
    rollingUpdate:
    maxSurge: 0
    maxUnavailable: 1
  type: RollingUpdate
```

**2. OnDelete アップデートストラテジを使うようにオブジェクトを編集します。これにより、Pod のいくつかを手動で強制終了できるようになり、それらが再作成されたら、イメージを更新します。**
```
student@setX-cp:~$ kubectl edit ds ds-one

....
  updateStrategy:
    rollingUpdate:
      maxUnavailable: 1
    type: OnDelete       #<-- この行を編集
status:
....
```

**3. nginx サーバの新しいバージョンを利用するようにDaemonSet を更新しましょう。今回はedit コマンドではなくset コマンドを利用します。バージョンは1.16.1-alpine に設定します。**
```
student@setX-cp:~$ kubectl set image ds ds-one nginx=nginx:1.16.1-alpine

daemonset.apps/ds-one image updated
```

**4. 前述のセクションで確認したPod のImage: パラメータは変わっていないことを確認します。**
```
student@setX-cp:~$ kubectl describe pod ds-one-b1dcv |grep Image:

Image: nginx:1.15.1
```

**5. Pod を削除します。代わりのPod が起動するまで待ち、もう一度バージョンを確認します。**
```
student@setX-cp:~$ kubectl delete pod ds-one-b1dcv

pod "ds-one-b1dcv" deleted
```
```
student@setX-cp:~$ kubectl get pod

NAME           READY   STATUS    RESTARTS   AGE
ds-one-xc86w   1/1     Running   0          19s  
ds-one-z31r4   1/1     Running   0          4m8s  
```
```
student@setX-cp:~$ kubectl describe pod ds-one-xc86w |grep Image:

Image: nginx:1.16.1-alpine
```

**6. 古い方のPod で実行しているイメージを確認します。バージョンはまだ1.15.1 と表示されるはずです。**
```
student@setX-cp:~$ kubectl describe pod ds-one-z31r4 |grep Image:

Image: nginx:1.15.1
```

**7. DaemonSet の変更履歴を確認します。2 件の履歴があるはずです。change-cause アノテーションを使っていなかったので、なぜこのオブジェクトを更新したのかわからなくなっています。**
```
student@setX-cp:~$ kubectl rollout history ds ds-one

daemonsets "ds-one"
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
```

**8. DaemonSet のそれぞれのバージョンの設定を確認します。2 つの出力内容の違いはImage: の行のみのはずです。**
```
student@setX-cp:~$ kubectl rollout history ds ds-one --revision=1

daemonsets "ds-one" with revision #1
Pod Template:
  Labels:       system=DaemonSetOne
  Containers:
   nginx:
    Image:      nginx:1.15.1
    Port:       80/TCP
    Host Port:  0/TCP
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>
```
```
student@setX-cp:~$ kubectl rollout history ds ds-one --revision=2

....
Image: nginx:1.16.1-alpine
.....
```

**9. kubectl rollout undo を利用してDaemonSet を過去のバージョンに戻しましょう。まだOnDelete 機能を利用しているので、Pod に変更はないはずです。**
```
student@setX-cp:~$ kubectl rollout undo ds ds-one --to-revision=1

daemonset.apps/ds-one rolled back 
```
```
student@setX-cp:~$ kubectl describe pod ds-one-xc86w |grep Image:

Image: nginx:1.16.1-alpine 
```

**10. Pod を削除し、代わりのPod が作成されるのを待ってから、もう一度イメージのバージョンを確認しましょう。**
```
student@setX-cp:~$ kubectl delete pod ds-one-xc86w

pod "ds-one-xc86w" deleted
```
```
student@setX-cp:~$ kubectl get pod

NAME           READY   STATUS      RESTARTS   AGE
ds-one-qc72k   1/1     Running     0          10s  
ds-one-xc86w   0/1     Terminating 0          12m  
ds-one-z31r4   1/1     Running     0          28m  
```
```
student@setX-cp:~$ kubectl describe pod ds-one-qc72k |grep Image:

Image: nginx:1.15.1
```

**11. DaemonSet の詳細を確認します。イメージがv1.15.1 と表示されるはずです。**
```
student@setX-cp:~$ kubectl describe ds |grep Image:

Image: nginx:1.15.1
```

**12. DaemonSet の現在の設定をYAML で出力します。。updateStrategy: のtype: を探します。**
```
student@setX-cp:~$ kubectl get ds ds-one -o yaml

apiVersion: apps/v1
kind: DaemonSet
.....
      terminationGracePeriodSeconds: 30
  updateStrategy:
    type: OnDelete
status:
  currentNumberScheduled: 2
.....
```

**13. アップデートポリシーをRollingUpdate に設定して、新しいDaemonSet を作成しましょう。まずは新しい設定ファイルを生成します。**
```
student@setX-cp:~$ kubectl get ds ds-one -o yaml > ds2.yaml
```
(テキストでは  --export  をつけていてこのバージョンではエラーになります。)　　

**14. ファイルを修正します。名前を ds-two に、 42 行目あたりのアップデートストラテジをデフォルトのRollingUpdate に変更します。**
```
student@setX-cp:~$ vim ds2.yaml

....  
name: ds-two  
....  
type: RollingUpdate 
```

[ds2.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/labfiles/ds2.yaml)


**15. 新しいDaemonSet を作成し、新しいPod のnginx のバージョンを確認します。**
```
student@setX-cp:~$ kubectl create -f ds2.yaml

daemonset.apps/ds-two created
```
```
student@setX-cp:~$ kubectl get pod

NAME           READY   STATUS    RESTARTS   AGE
ds-one-qc72k   1/1     Running   0          28m  
ds-one-z31r4   1/1     Running   0          57m  
ds-two-10khc   1/1     Running   0          5m  
ds-two-kzp9g   1/1     Running   0          5m  
```
```
student@setX-cp:~$ kubectl describe pod ds-two-10khc |grep Image:

Image: nginx:1.15.1 
```

**16. 設定ファイルを修正し、新しいバージョンに設定しましょう。**
```
student@setX-cp:~$ kubectl edit ds ds-two

....  
  - image: nginx:1.16.1-alpine  
.....
``` 

**17. DaemonSet の作成時からの経過時間を確認します。確認したタイミングによりますが、作成時からの経過時間は10 分程になっているはずです。**
```
student@setX-cp:~$ kubectl get ds ds-two

NAME     DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
ds-two   2         2         2       2            2           <none>          10m
```

**18. 今度はPod の作成時からの経過時間を確認します。2 つのPod がDaemonSet より直近に作成されているはずです。また、ローリングアップデートではPod を1 つずつ停止・再作成するため、2 つのPod の間に数秒の差があるはずです。**
```
student@setX-cp:~$ kubectl get pod

NAME           READY   STATUS    RESTARTS   AGE
ds-one-qc72k   1/1     Running   0          36m  
ds-one-z31r4   1/1     Running   0          1h  
ds-two-2p8vz   1/1     Running   0          34s  
ds-two-8lx7k   1/1     Running   0          32s  
```

**19. Pod がソフトウェアの新しいバージョンを利用していることを確認します。**
```
student@setX-cp:~$ kubectl describe pod ds-two-8lx7k |grep Image:

Image: nginx:1.16.1-alpine
```

**20. ロールアウトのステータスとDaemonSet の履歴を確認します。**
```
student@setX-cp:~$ kubectl rollout status ds ds-two

daemon set "ds-two" successfully rolled out 
```
```
student@setX-cp:~$ kubectl rollout history ds ds-two 

daemonsets "ds-two"  
REVISION  CHANGE-CAUSE
1         <none>
2         <none>

```

**21. 更新の変更点を確認すると、先に確認した履歴と同じであるはずです。今回は更新の実行にPod の削除を必要としませんでした。**
```
student@setX-cp:~$ kubectl rollout history ds ds-two --revision=2

...  
Image: nginx:1.16.1-alpine 
```

**22. 最後にDaemonSet を削除します。**
```
student@setX-cp:~$ kubectl delete ds ds-one ds-two

daemonset.apps "ds-one" deleted  
daemonset.apps "ds-two" deleted
```


以上  

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)

