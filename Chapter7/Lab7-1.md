# 課題7.1: ReplicaSet の利用

**概要**

コンテナの状態を把握し、管理することはKubernetes の主要タスクです。この演習では、まずコンテナのグループを管理するために利用されるAPI オブジェクトを見てみましょう。Kubernetes の開発が進むにつれて、利用可能なオブジェクトも変わったため、利用しているKubernetes のバージョンによってどのようなオブジェクトが利用可能であるかが異なります。最初に見ていくオブジェクトはReplicaSet です。ReplicaSet には、Deployment に含まれるいくつかの新しい管理機能がありません。Deployment オペレータがあなたに代わってReplicaSet オペレータを管理します。さらに、新しく追加したノードにコンテナを起動するDaemonSet という監視ループも利用します。  
次に、コンテナのソフトウェアを更新し、修正履歴を見て過去のバージョンにロールバックします。
<br>
<br>
ReplicaSet はselector をサポートする、次世代Replication Controller といえるものです。今では、通常の処理では行えないオーケストレーションの更新の場合、もしくはコンテナソフトウェアの更新が不要な場合のみ、ReplicaSet を使用します。


**1. ReplicaSet があるかどうかを確認しましょう。前回の演習の終了時にリソースを削除したのであれば、default namespaceにはReplicaSet は存在しないはずです。**
```
student@setX-cp:~$ kubectl get rs

No resources found in default namespace.
```

**2. 簡単なReplicaSet 用のYAML ファイルを作成しましょう。Kubernetes のバージョンによって、apiVersion の設定は異なります。このオブジェクトはapps/v1 apiVersion を使っている安定版です。nginx に関しては、まずは古いバージョンを使い、後ほど新しいバージョンに更新します。**
```
student@setX-cp:~$ vim rs.yaml
```
![chap7-1](/uploads/7cb0f050d40e3582792729a93c0d6ad8/chap7-1.png)

[rs.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/labfiles/rs.yaml)


**3. ReplicaSet を作成します。**
```
student@setX-cp:~$ kubectl create -f rs.yaml

replicaset.apps/rs-one created
```

**4. 新しく作成したReplicaSet を確認します。**
```
student@setX-cp:~$ kubectl describe rs rs-one

Name:         rs-one
Namespace:    default
Selector:     system=ReplicaOne
Labels:       <none>
Annotations:  <none>
Replicas:     2 current / 2 desired
Pods Status:  0 Running / 2 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  system=ReplicaOne
  Containers:
   nginx:
    Image:        nginx:1.15.1
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
```

**5. ReplicaSet が作成したPod を確認します。先程作成したYAML ファイルでは、2 つのPod を作成しているはずです。Completed 状態のbusybox コンテナがあるかもしれませんが、これはいずれ消えます。**
```
student@setX-cp:~$ kubectl get pods

NAME           READY   STATUS    RESTARTS   AGE
rs-one-2p9x4   1/1     Running   0          5m4s  
rs-one-3c6pb   1/1     Running   0          5m4s  
```

**6. ReplicaSet を削除しましょう。ReplicaSet が管理していたPod は残しておきます。**
```
student@setX-cp:~$ kubectl delete rs rs-one --cascade=false

warning: --cascade=false is deprecated (boolean value) and can be replaced with --cascade=orphan.
replicaset.apps "rs-one" deleted

ワーニングが表示されますが削除はされています。
```

**7. もう一度ReplicaSet とPod を確認します。**
```
student@setX-cp:~$ kubectl describe rs rs-one

Error from server (NotFound): replicasets.apps "rs-one" not found
```
```
student@setX-cp:~$ kubectl get pods  

NAME           READY   STATUS    RESTARTS   AGE
rs-one-2p9x4   1/1     Running   0          7m  
rs-one-3c6pb   1/1     Running   0          7m  
```

**8. 再度ReplicaSet を作成します。selector フィールドを変えない限り、Pod は新しいReplicaSet に紐付けられます。この方法ではPod のバージョンは更新できません。**
```
student@setX-cp:~$ kubectl create -f rs.yaml

replicaset.apps/rs-one created
```

**9. ReplicaSet と、それに紐づくPod のAGE（作成時からの経過時間）を確認します。**
```
student@setX-cp:~$ kubectl get rs

NAME     DESIRED   CURRENT   READY   AGE
rs-one   2         2         2       46s
```
```
student@setX-cp:~$ kubectl get pods

NAME           READY   STATUS    RESTARTS   AGE
rs-one-2p9x4   1/1     Running   0          8m  
rs-one-3c6pb   1/1     Running   0          8m  
```

**10. 次にPod をReplicaSet から分離しましょう。まずPod のLabel を修正し、system: の値をIsolatedPod に変更します。**
```
student@setX-cp:~$ kubectl edit pod rs-one-3c6pb

....
  labels:
    system: IsolatedPod   #<-- ReplicaOne から IsolatedPod に変更
managedFields:
....
```

**11. ReplicaSet の中のPod 数を確認します。2 つ起動しているはずです。**
```
student@setX-cp:~$ kubectl get rs  

NAME     DESIRED   CURRENT   READY   AGE
rs-one   2         2         2       4m
```

**12. ラベルkey にsystem が設定されているPod を表示します。3 つのPod が表示され、そのうちの1 つは他のPod より新しく作成されているはずです。system ラベルがIsolated のPod の他に、ReplicaSet は常にPod のレプリカが2 つある状態を保ちます。**
```
student@setX-cp:~$ kubectl get pod -L system

NAME           READY   STATUS    RESTARTS   AGE    SYSTEM
rs-one-3c6pb   1/1     Running   0          10m    IsolatedPod  
rs-one-2p9x4   1/1     Running   0          10m    ReplicaOne  
rs-one-dq5xd   1/1     Running   0          30s    ReplicaOne  
```

**13. ReplicaSet を削除し、残っているPod を確認します。**
```
student@setX-cp:~$ kubectl delete rs rs-one

replicaset.apps "rs-one" deleted
```
```
student@setX-cp:~$ kubectl get po 

NAME           READY   STATUS        RESTARTS   AGE
rs-one-3c6pb   1/1     Running       0          14m  
rs-one-dq5xd   0/1     Terminating   0          4m  
```

**14. この例では、Pod が停止処理をまだ終えていない状態です。少し待ってからもう一度確認します。ReplicaSet が管理していたPod は削除され、Isolated のPod が1 つ残っているはずです。**
```
student@setX-cp:~$ kubectl get rs

No resources found in default namespaces. 
```
```
student@setX-cp:~$ kubectl get pod

NAME           READY   STATUS    RESTARTS   AGE
rs-one-3c6pb   1/1     Running   0          16m  
```

**15. Label を指定して、残っているPod を削除します。**
```
student@setX-cp:~$ kubectl delete pod -l system=IsolatedPod

pod "rs-one-3c6pb" deleted
```
  

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-2.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)

