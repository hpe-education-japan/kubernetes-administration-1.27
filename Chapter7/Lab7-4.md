# 課題7.4: DaemonSet の利用

DaemonSet は、他の演習で利用してきたDeployment のような監視ループを持つオブジェクトです。DaemonSet は、クラスタにノードを追加するたびに、そのノード上にPod を作成します。Deployment はクラスタ全体に作成するPod の合計数のみを管理するため、1つのノードには複数作成し、あるノードには1 つも作成しないということが起こりえます。アプリケーションが各ノードにあることを確認するために、DaemonSet を使用することは、特にハードウェアが頻繁に交換されるかもしれない大規模なクラスタでのメトリクスやロギングのようなもののために役立ちます。クラスタからノードを削除する場合、DaemonSet はその前にPod をガベージコレクション（削除）します。 
<br>
 
v1.12 からは、DaemonSet のデプロイはスケジューラが管理するため、あるノードには特定のDaemonSet のPod を配置しないように設定することも可能になりました。  
この自動化の仕組みは、ceph のようなハードウェアの一部であるストレージの追加や削除を頻繁に行う製品に役立ちます。メモリ、CPU、Volume のように宣言的に割り当てるリソースと組み合わせることによって、複雑なデプロイも可能になります。  

**1. まず、YAML ファイルを作成しましょう。この場合は、kind をDaemonSet に設定します。作業を簡略化するために、事前に作成してあったrs.yamlファイルをコピーして、いくつかの修正を加えましょう。Replicas: 2 の行を削除します。**
```
student@setX-cp:~$ cp rs.yaml ds.yaml
student@setX-cp:~$ vim ds.yaml
```

![chap7-2](/uploads/64cb53689fa1b623f8737e19e53e9a46/chap7-2.png)

[ds.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/labfiles/ds.yaml)


**2. DaemonSet を作成し、状態を確認します。クラスタ内のノード1 つにつきPod が1 つあるはずです。**
```
student@setX-cp:~$ kubectl create -f ds.yaml

daemonset.apps/ds-one created
```
```
student@setX-cp:~$ kubectl get ds

NAME     DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
ds-one   2         2         2       2            2           <none>          1m
```
```
student@setX-cp:~$ kubectl get pod 

NAME           READY   STATUS    RESTARTS   AGE
ds-one-b1dcv   1/1     Running   0          2m  
ds-one-z31r4   1/1     Running   0          2m  
```

**3. Pod 内で実行しているイメージを確認しましょう。この情報は次のセクションで利用します。**
```
student@setX-cp:~$ kubectl describe po ds-one-b1dcv | grep Image:

Image: nginx:1.15.1
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-5.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)

