# 課題7.2: Deployment の利用

Deployment は、これまでの演習で扱ってきたウォッチループ・オブジェクトです。Deployment は Pod と ReplicaSet の宣言的な更新を可能にし、一般的に指定された数の Pod が作成されることを保証します。いくつかの Pod は単一のノードに作成される場合もあります。Deployment はハイレベルなリソースオブジェクトで、コンテナ化されたアプリケーションのロールアウトおよびスケーリングを管理します。Deployment は、レプリカの数や利用するコンテナイメージといった、アプリケーションの望ましい状態を記述します。  
Deployment が作成されると、Kubernetes は自動的に必要な ReplicaSet を作成・管理します。ReplicaSet は、アプリケーションの望ましい状態を満たすよう、必要な Pod を作成・管理します。Deployment はローリングアップデートを提供し、古いレプリカを順に新しいもので置き換えることで、ダウンタイム無しでアプリケーションを新しいバージョンに更新することを可能にします。 
 Kubernetes にてDeployment を使うことで、コンテナ化されたアプリケーションを簡単に管理・スケールしながら高い可用性や信頼性を実現できます。   
<br>

**1. はじめに yaml ファイルを作成します。ここでは、kind を deployment に設定します。yaml ファイルは Imperative メソッドを使って生成できます。**
```
student@setX-cp:~$  kubectl create deploy webserver --image nginx:1.22.1 --replicas=2 \
--dry-run=client -o yaml | tee dep.yaml

student@setX-cp:~$ cat dep.yaml
```

![chap7-3](/uploads/306b382d3dd1ec0654dbce0b52668e3d/chap7-3.png)


**2. 新しい Deployment を作成し、検証します。クラスタ内に Pod の Replica が 2 つ作成されるはずです。**
```
student@setX-cp:~$ kubectl create -f dep.yaml


deployment.apps/webserver created
```
```
student@setX-cp:~$ kubectl get deploy


NAME        READY   UP-TO-DATE   AVAILABLE   AGE
webserver   2/2     2            2           10s
```
```
student@setX-cp:~$ kubectl get pod 


NAME                         READY   STATUS    RESTARTS   AGE
webserver-6cbc654ddc-lssbm   1/1     Running   0          42s
webserver-6cbc654ddc-xpmtl   1/1     Running   0          42s  
```

**3. Pod 内で動作するイメージを検証します。この情報は次のセクションで利用します。**
```
student@setX-cp:~$ kubectl describe pod webserver-6cbc654ddc-lssbm | grep Image:


Image: nginx:1.22.1
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-3.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)

