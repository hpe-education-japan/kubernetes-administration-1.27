# 課題7.3: ローリングアップデートとロールバック

マイクロサービスの利点の一つに、クライアントからのリクエストに応答し続けながら、コンテナを置き換え更新することができることがあります。recreate 設定を利用し、既存のコンテナが削除された際にコンテナをアップグレードします。またローリングアップデートを起用してローロングアップデートを即時に開始します。  


**nginx のバージョン**

nginx ソフトウェアは Kubernetes とは異なるタイムラインで更新されます。演習に書かれたバージョンが古い場合は、現在のデフォルト、そしてより新しいバージョンを利用してください。バージョンはレジストリのリポジトリで確認できます。  
  
<br>

**1. はじめに、前のセクションで作成した Deployment の現在の strategy 設定を見てみましょう。**
```
student@setX-cp:~$ kubectl get deploy webserver -o yaml | grep -A 4 strategy


  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate

```

**2.  Recreate update strategy を使うようオブジェクトを編集します。これにより一部の Pod を手動で停止し、更新したイメージで再作成することができます。**
```
student@setX-cp:~$ kubectl edit deploy webserver


....
strategy:
rollingUpdate: # <-- この行は削除
  maxSurge: 25% # <-- この行は削除
  maxUnavailable: 25% # <-- この行は削除
type: Recreate # <-- この行を編集
....

```

**3. Deployment を更新し、新しいバージョンの nginx サーバを利用するようにします。ここでは、edit ではなく set コマンドを使いましょう。バージョンを1.23.1-alpine に設定してください。**
```
student@setX-cp:~$ kubectl set image deploy webserver nginx=nginx:1.23.1-alpine


deployment.apps/webserver image updated
```

**4. 前のセクションで確認した Pod の Image: パラメタが変更されていないことを確認します。**
```
student@setX-cp:~$ kubectl get pod

NAME                       READY STATUS  RESTARTS AGE
webserver-6cf9cd5c74-qjph4 1/1   Running 0        35s

```
```
student@setX-cp:~$ kubectl describe po webserver-6cf9cd5c74-qjph4 |grep Image:


Image: nginx:1.23.1-alpine
```

**5. Deployment の変更履歴を確認します。2 つのリビジョンが表示されているはずです。change-cause annotation を追加していないので、オブジェクトが更新された理由は表示されません。**
```
student@setX-cp:~$ kubectl rollout history deploy webserver


deployment.apps/webserver
REVISION CHANGE-CAUSE
1        <none>
2        <none>
```

**6. 様々なバージョンの Deployment の設定を確認します。Image: 行のみが 2 つの出力の差になります。**
```
student@setX-cp:~$ kubectl rollout history deploy webserver --revision=1


deployment.apps/webserver with revision #1
Pod Template:
  Labels: app=webserver
    pod-template-hash=6cbc654ddc
  Containers:
    nginx:
    Image:        nginx:1.22.1
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
```
```
student@setX-cp:~$ kubectl rollout history deploy webserver --revision=2


....
    Image: nginx:1.23.1-alpine
.....

```

**7. kubectl rollout undo を使って、Deployment を以前のバージョンに戻します。**
```
student@setX-cp:~$ kubectl rollout undo deploy webserver


deployment.apps/webserver rolled back
```
```
student@setX-cp:~$ kubectl get pod


NAME                       READY STATUS  RESTARTS AGE
webserver-6cbc654ddc-7wb5q 1/1   Running 0        37s
webserver-6cbc654ddc-svbtj 1/1   Running 0        37s
```
```
student@setX-cp:~$ kubectl describe pod webserver-6cbc654ddc-7wb5q |grep Image:


  Image: nginx:1.22.1

```

**8. Deployment を削除し、システムをクリーンナップします。**
```
student@setX-cp:~$  kubectl delete deploy webserver


deployment.apps "webserver" deleted
```

以上  

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-4.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)

