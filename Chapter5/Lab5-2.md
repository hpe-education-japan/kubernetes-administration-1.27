# 課題5.2: API 呼び出しを試しましょう

**1. コマンドが何を行っているのかを調べるにはstrace を利用できます。今回は、現在のエンドポイント、つまりAPI 呼び出しの対象について調べます。strace がインストールされていなければ、インストールしましょう。(本環境ではインストール済みです)**
```
student@setX-cp:~$ sudo apt-get install -y strace
student@setX-cp:~$ kubectl get endpoints

NAME         ENDPOINTS        AGE
kubernetes   10.128.0.3:6443  3h
```

**2. コマンドをstrace を使って再度実行します。出力結果は多くなるはずです。最後の方に、ローカルディレクトリ（/home/student/.kube/cache/discovery/k8scp_6443）へのopenat 機能が複数表示されているはずです。見つからない場合は、出力をすべてファイルにリダイレクト（strace -o file）し、grep して探しましょう。この情報はキャッシュされているため、コマンドを何度か実行する必要があるかもしれません。同様にIP アドレスも異なる場合があります。**
```
student@setX-cp:~$ strace kubectl get endpoints

execve("/usr/bin/kubectl", ["kubectl", "get", "endpoints"], [/*....
....
openat(AT_FDCWD, "/home/student/.kube/cache/discovery/k8scp_6443..
< 省略>
```

**3. 出力されたディレクトリの親ディレクトリに移動し、次のコマンドを実行します。**
```
student@setX-cp:~$ cd /home/student/.kube/cache/discovery/
student@setX-cp:~/.kube/cache/discovery$ ls

k8scp_6443
```
```
student@setX-cp:~/.kube/cache/discovery$ cd k8scp_6443/
```

**4. 内容を確認しましょう。Kubernetes のさまざまな設定情報を含むディレクトリが確認できるはずです。**
```
student@setX-cp:~/.kube/cache/discovery/k8scp_6443$ ls

admissionregistration.k8s.io certificates.k8s.io          node.k8s.io
apiextensions.k8s.io         coordination.k8s.io          policy
apiregistration.k8s.io       crd.projectcalico.org        rbac.authorization.k8s.io
apps                         discovery.k8s.io             scheduling.k8s.io
authentication.k8s.io        events.k8s.io                servergroups.json
authorization.k8s.io         extensions                   storage.k8s.io
autoscaling                  flowcontrol.apiserver.k8s.io v1
batch                        networking.k8s.io
```

**5. find コマンドでディレクトリ内のすべてのファイルを表示します。このページではわかりやすくするためにプロンプトを修正しています。**
```
student@setX-cp:./k8scp_6443$ find .

./batch  
./batch/v1  
./batch/v1/serverresources.json  
./batch/v1beta1  
./batch/v1beta1/serverresources.json  
./apiextensions.k8s.io  
./apiextensions.k8s.io/v1  
./apiextensions.k8s.io/v1/serverresources.json  
./apiextensions.k8s.io/v1beta1  
./apiextensions.k8s.io/v1beta1/serverresources.json  
< 省略> 
```

**6. API バージョン1 で利用可能なオブジェクトを見てみましょう。各オブジェクトやkind: キーで利用できるverb(動詞) や動作を確認できるはずです。例えば、次の例では「create」というverb が利用できます。プロンプトは、コマンドが一行に収まるように切り
詰められていることに注意してください。GET のようにHTTP メソッドと同じようなverb もありますが、標準的なHTTP メソッドではないオブジェクト特有のverb もあります。オブジェクトを表示するコマンドにはpythonが利用できます。**
```
student@setX-cp:.$ python3 -m json.tool v1/serverresources.json
```

![chap5-3](/uploads/4371ffdaa486275ed099a3a670b6cb4e/chap5-3.png)


**7. 多くのオブジェクトにはshortName（略称）があります。これによってコマンドラインでの利用が容易になります。Endpoint を表示するためのshortName を見つけましょう。**
```
student@setX-cp:.$ python3 -m json.tool v1/serverresources.json | less
```

![chap5-1](/uploads/577de3c354b8d4ceae645645943b0b1a/chap5-1.png)


**8. shortName を利用してエンドポイントを表示させます。先ほどのコマンドの出力内容と一致するはずです。**
```
student@setX-cp:.$ kubectl get ep

NAME         ENDPOINTS        AGE
kubernetes   10.128.0.3:6443  3h
```

**9. バージョン1 のファイルには、37 のオブジェクトがあることがわかります。**
```
student@setX-cp:.$ python3 -m json.tool v1/serverresources.json | grep kind

    "kind": "APIResourceList",
            "kind": "Binding",
            "kind": "ComponentStatus",
            "kind": "ConfigMap",
            "kind": "Endpoints",
            "kind": "Event",
< 省略>
```

**10. 他のファイルを見ると、9 個表示されます。**
```
student@setX-cp:$ python3 -m json.tool apps/v1/serverresources.json | grep kind

    "kind": "APIResourceList",
            "kind": "ControllerRevision",
            "kind": "DaemonSet",
            "kind": "DaemonSet",
            "kind": "Deployment",
< 省略>
```

**11. システムリソースを解放するため、curlpod を削除します。$HOMEに戻ります。**
```
student@setX-cp:$ kubectl delete po curlpod

pod "curlpod" deleted

student@setX-cp:$ cd $HOME
```  

**12. 時間があるようであれば、このディレクトリ内の他のファイルも確認しましょう。**

以上  

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
