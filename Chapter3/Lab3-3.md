# 課題3.3: クラスタのセットアップの仕上げ


**1. クラスタの有効なノードを確認します。1～2 分でステータスがNotReady からReady に変わります。NAME フィールドは詳細を確認するために使用します。あなたのノード名とは異なっているはずです。**
```
student@setX-master:~$ kubectl get node

NAME          STATUS   ROLES           AGE   VERSION
set0-cp       Ready    control-plane   13m   v1.27.11
set0-worker   Ready    <none>          72s   v1.27.11
```

**2. ノードの詳細を確認します。リソースとその現在のステータスを各行ごとに表示しています。Taints のステータスに注意しましょう。デフォルトでは、マスタノードではKubernetes システムに必要なPod の起動のみを許可します。リソース競合やセキュリティ上の理由によります。少し時間を取って、各出力の行を確認しましょう。いくつかはエラーとなっているかもしれませんが、そのうちステータスがFalse になるでしょう。**
```
student@setX-master:~$ kubectl describe node setX-cp

Name:              setX-cp
Roles:             control-plane,master
Labels:            beta.kubernetes.io/arch=amd64
                   beta.kubernetes.io/os=linux
                   kubernetes.io/arch=amd64
                   kubernetes.io/hostname=cp
                   kubernetes.io/os=linux
                   node-role.kubernetes.io/control-plane=
                   node-role.kubernetes.io/master=
Annotations:       kubeadm.alpha.kubernetes.io/cri-socket: /var/run/dockershim.sock
                   node.alpha.kubernetes.io/ttl: 0
                   projectcalico.org/IPv4Address: 10.142.0.3/32
                   projectcalico.org/IPv4IPIPTunnelAddr: 192.168.242.64
                   volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp: Wed, 26 May 2021 22:04:03 +0000
Taints:            node-role.kubernetes.io/master:NoSchedule
```

**3. では、コントロールプレーンサーバがKubernetes システムに必要ではないPod を実行することを許可しましょう。マスタノードはセキュリティとパフォーマンス上の理由から、Taint を持って起動しています。トレーニング環境としてマスタノードを利用できるようにしましょう。なお、本番環境ではこの手順を省いたほうがよいでしょう。末尾のマイナス記号(-) はtaint を削除するための書式です。2 番目のノードはtaint を有さないため、not found エラーとなりますが問題ありません。。複数の Taint があるかもしれません。全てがなくなるまで、確認・削除を繰り返します。**
```
student@setX-master:~$ kubectl describe node | grep -i taint

Taints:         node-role.kubernetes.io/master:NoSchedule
Taints:         <none>
```
```
student@setX-master:~$ kubectl taint nodes --all node-role.kubernetes.io/control-plane-

node/master untainted  
error: taint "node-role.kubernetes.io/control-plane" not found 
```

**4. DNS とCalico の使用準備が完了しているかどうかを確認します。すべてのステータスがRunning となっているはずです。Pending からの移行には1～2 分かかる場合もあります。**  
```
student@setX-master:~$ kubectl get pods --all-namespaces

NAMESPACE   NAME                                     READY STATUS  RESTARTS AGE
kube-system calico-node-jlgwr                        1/1   Running 0        6m
kube-system calico-kube-controllers-74b888b647-wlqf5 1/1   Running 0        6m
kube-system calico-node-tpvnr                        2/2   Running 0        6m
kube-system coredns-78fcdf6894-nc5cn                 1/1   Running 0        17m
kube-system coredns-78fcdf6894-xs96m                 1/1   Running 0        17m
< 省略>
```

**5. coredns- Pod がContainerCreating ステータスで止まっている場合のみ、新しくPod を作成できるようにそれらを削除する必要があるかもしれません。両方のPod を削除し、Running ステータスになるかどうかを確認します。なお、Pod の名前はあなたのものとは異なります。**
```
student@setX-master:~$ kubectl get pods --all-namespaces

NAMESPACE   NAME                     READY STATUS            RESTARTS AGE
kube-system calico-node-qkvzh        2/2   Running           0        59m
kube-system calico-node-vndn7        2/2   Running           0        12m
kube-system coredns-576cbf47c7-rn6v4 0/1   ContainerCreating 0        3s
kube-system coredns-576cbf47c7-vq5dz 0/1   ContainerCreating 0        94m
< 省略>
```
```
student@setX-master:~$ kubectl -n kube-system delete pod coredns-576cbf47c7-vq5dz coredns-576cbf47c7-rn6v4

pod "coredns-576cbf47c7-vq5dz" deleted  
pod "coredns-576cbf47c7-rn6v4" deleted
```

**6. 完了したら、新しいトンネルのtunl0 やcali インターフェイスが確認できるはずです。これには1 分ほどかかるかもしれません。オブジェクトを作成するにつれ、より多くのインターフェイスが作成されます。Pod をデプロイした際に次の出力に表示されるcali インターフェイスがそれに該当します。**
```
student@setX-master:~$ ip a

< 省略>
4: tunl0@NONE: <NOARP,UP,LOWER_UP> mtu 1440 qdisc noqueue state
UNKNOWN group default qlen 1000
    link/ipip 0.0.0.0 brd 0.0.0.0
    inet 192.168.0.1/32 brd 192.168.0.1 scope global tunl0
      valid_lft forever preferred_lft forever
6: calib0b93ed4661@if4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu
1440 qdisc noqueue state UP group default
    link/ether ee:ee:ee:ee:ee:ee brd ff:ff:ff:ff:ff:ff link-netnsid 1
    inet6 fe80::ecee:eeff:feee:eeee/64 scope link
      valid_lft forever preferred_lft forever
< 省略>
```

**7. Containerd はruntime-endpoint の古い記法を未だ利用している場合があります。unix//: のような宣言されていないリソースタイプに関するエラーが見られるかもしれません。crictl 設定を更新します。設定可能な内容が多数あります。一つを設定し、作成された設定ファイルを確認します。**
```
student@setX-master:~$ sudo crictl config --set runtime-endpoint=unix:///run/containerd/containerd.sock --set image-endpoint=unix:///run/containerd/containerd.sock

student@setX-master:~$ sudo cat /etc/crictl.yaml

runtime-endpoint: "unix:///run/containerd/containerd.sock"
image-endpoint: "unix:///run/containerd/containerd.sock"
timeout: 0
debug: false
pull-image-on-create: false
disable-pull-on-run: false
```


以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/Lab3-4.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/tree/main)

