# 課題3.5: クラスタ外からアクセス


DNS アドオンや環境変数を利用してクラスタの外からService にアクセスすることができます。ここでは、環境変数を利用してPod にアクセスしてみます。

**1. まずはPod の一覧を確認しましょう。**
```
student@setX-master:~$ kubectl get po

NAME                    READY   STATUS    RESTARTS   AGE
nginx-1423793266-13p69  1/1     Running   0          4m10s  
nginx-1423793266-8w2nk  1/1     Running   0          8m2s  
nginx-1423793266-fbt4b  1/1     Running   0          8m2s
```

**2. Pod をどれか1 つ選んで、exec コマンドを利用してPod 内でprintenv を実行します。次の例では、1 つ目のPod を選んでいます。**
```
student@setX-master:~$ kubectl exec nginx-1423793266-13p69 -- printenv |grep KUBERNETES

KUBERNETES_SERVICE_PORT=443  
KUBERNETES_SERVICE_HOST=10.96.0.1  
KUBERNETES_SERVICE_PORT_HTTPS=443  
KUBERNETES_PORT=tcp://10.96.0.1:443  
< 省略>
```

**3. nginx 用の既存のService を見つけて削除します。**
```
student@setX-master:~$ kubectl get svc

NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1       <none>        443/TCP   4h  
nginx        ClusterIP   10.100.61.122   <none>        80/TCP    17m  
```

**4. Service を削除します。**
```
student@setX-master:~$ kubectl delete svc nginx

service "nginx" deleted
```

**5. 再度Service を作成します。今回はService Type にLoadBalancer を指定します。ステータスと外部ポートを確認しましょう。External-IP がpending になっているはずです。クラウドプロバイダのロードバランサが応答しない限り、pending のままになります。**
```
student@setX-master:~$ kubectl expose deployment nginx --type=LoadBalancer

service/nginx exposed
```
```
student@setX-master:~$ kubectl get svc

NAME         TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
Kubernetes   ClusterIP      10.96.0.1      <none>        443/TCP        4h  
nginx        LoadBalancer   10.104.249.102 <pending>     80:32753/TCP   6s  
```

**6. 演習用ノードではなくローカルシステムでブラウザを開き、ノードのパブリックIP アドレスと先の出力にある32753 番ポートを利用しましょう。AWS やGCE のようなリモートシステムで演習を実施しているなら、PuTTY やSSH でのアクセス先のパブリックIP アドレスを使います。curl によって IP アドレスが検出できるかもしれません。**
```
student@setX-master:~$ curl ifconfig.io
54.214.214.156

```

![chap3-8](/uploads/512772f492511329efe7b3a351578aad/chap3-8.png)

**7. Deployment のレプリカ数を0 にします。そして、再度ウェブページを確認します。すべてのPod が終了したら、ウェブサイトにアクセスできなくなります。**
```
student@setX-master:~$ kubectl scale deployment nginx --replicas=0

deployment.apps/nginx scaled 
```
```
student@setX-master:~$ kubectl get po

No resources found in default namespace.
```

**8. レプリカが2 つになるようにDeployment をスケールします。再度ウェブページへのアクセスが成功するようになるはずです。**
```
student@setX-master:~$ kubectl scale deployment nginx --replicas=2

deployment.apps/nginx scaled
```
```
student@setX-master:~$ kubectl get po

NAME                    READY   STATUS    RESTARTS   AGE
nginx-1423793266-7x181  1/1     Running   0          6s  
nginx-1423793266-s6vcz  1/1     Running   0          6s
```

**9. システムリソースを回復するため、Deployment を削除します。deployment を削除しても、エンドポイントやService は削除されないことに注意しましょう。**
```
student@setX-master:~$ kubectl delete deployments nginx

deployment.apps "nginx" deleted
```
```
student@setX-master:~$ kubectl delete ep nginx

endpoints "nginx" deleted
```
```
student@setX-master:~$ kubectl delete svc nginx  

service "nginx" deleted
```
  

以上

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/tree/main)