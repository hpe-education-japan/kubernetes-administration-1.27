# 課題3.2: クラスタの拡張

別の端末を開き、2 番目のノードに接続します。 containerd と Kubernetes ソフトウェアをインストールします。 これらは、cp ノードで実行した手順の多くですが、すべてではありません。  
本書では、各ノード用のコマンドを正しく実行できるように、ここで追加するノードにはsetX-worker をプロンプトとして使用します。  
注意：注意：プロンプトは、ユーザとコマンドを実行するシステムの両者を示します。正しいノードを把握するために、ターミナルセッションの色や文字を変更するのも有用です。  

**1. 以前と同じ手順で、2 つ目のノードに接続します。**

講師と対面でのトレーニングの場合、新しいノードには前と同じ.pemキーと、講師が提供する新しいIP アドレスを利用しましょう。見分けをつけるために、新しいターミナルウィンドウに1 つ目と異なる名前と色を与えるとよいでしょう。プロンプトが似ているためです。  

**2. **
```
student@setX-worker:~$ sudo -i
```
**3. **
```
root@setX-worker:~# apt-get update && apt-get upgrade -y
```
< 質問された場合、サービスの再起動を許可し、現在ローカルにインストールされているバージョンを維持します>  

**4. containerd エンジンをインストールし、依存ソフトウェアとともに起動します。**
```
root@setX-worker:~# apt install curl apt-transport-https vim git wget \
software-properties-common lsb-release ca-certificates -y

root@setX-worker:~# swapoff -a
root@setX-worker:~# modprobe overlay
root@setX-worker:~# modprobe br_netfilter

root@setX-worker:~# cat << EOF | tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

root@setX-worker:~# sysctl --system

root@setX-worker:~# mkdir -p /etc/apt/keyrings
root@setX-worker:~# curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
| sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

root@setX-worker:~#  echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

root@setX-worker:˜# apt-get update && apt-get install containerd.io -y
root@setX-worker:˜# containerd config default | tee /etc/containerd/config.toml
root@setX-worker:˜# sed -e 's/SystemdCgroup = false/SystemdCgroup = true/g' -i /etc/containerd/config.toml
root@set-Xworker:˜# systemctl restart containerd
```
**5. Kubernetes リポジトリを追加します**
```
root@set-Xworker:˜# echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.27/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
```

**6. ソフトウェアのための GPG 鍵を取得します**
```
root@setX-worker:~# curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.27/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
```

**7. リポジトリを更新し、Kubernetes ソフトウェアをインストールします。コントロールプレーンのバージョンと必ず一致させて下さい。**
```
root@setX-worker:~# apt-get update
```

**8.**  
```
root@setX-worker:~# apt-get install -y kubeadm kubelet kubectl
```

**9. システム更新時にバージョンが維持されるようにします**
```
root@setX-worker:~# apt-mark hold kubeadm kubelet kubectl
```

**10. コントロールプレーンサーバのIP アドレスを見つけます。**

インターフェイスの名前は、ノードが起動している場所によって変わります。例ではGCE で起動しているので、このノードのプライマリインターフェイスはens4 です。受講者のインターフェイスの名前は異なるかもしれません。出力内容からマスタノードのIP アドレスは10.128.0.3 だということがわかります。 
```
student@setX-master:~$ hostname -i

10.128.0.3
``` 
```
student@setX-master:~$ ip addr show ens4 | grep inet

  inet 10.128.0.3/32 brd 10.128.0.3 scope global ens4  
  inet6 fe80::4001:aff:fe8e:2/64 scope link  
```

**11. この時点で、join コマンドをコントロールプレーンノードからコピー＆ペーストしましょう。**

このコマンドは2 時間のみ有効なので、将来的には独自のjoin をビルドしノードを追加する必要があります。マスタノードのトークンを見つけます。このトークンはデフォルトで2 時間有効です。有効期限を過ぎてしまったり、トークンを紛失した場合、次の次に示すsudo kubeadm token create コマンドで新しいトークンを作成できます。  

```
studentsetX-master:~$ sudo kubeadm token list

TOKEN                   TTL EXPIRES              USAGES                 DESCRIPTION  EXTRA GROUPS
bml44w.3owxl50rrtymamt7 2h  2023-05-27T18:49:41Z authentication,signing <none>       system:bootstrappers:kubeadm:default-node-token  
```

**12. 2 時間以上後にノードを追加することになり、join コマンドの一部として使用するため新しくトークンを作成することになったと想定しましょう。**
```
studentsetX-master:~$ sudo kubeadm token create

27eee4.6e66ff60318da929
```

**13. ノードを安全にクラスタに追加することを保証するために、マスタノードでDiscovery Token CA Cert Hash を作成し、利用しましょう。これはマスタノード、あるいはCA ファイルのコピーがある場所で実行します。長い出力内容を表示するはずです。**

PDF からコピーペーストした場合、コマンド末尾の CA 証明書 (^) とシングルクォート (') が問題になる場合があります。  

```
studentsetX-mster:~$ openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'


6d541678b05652e1fa5d43908e75e67376e994c3483d6683f2a18673e5d2a1b0
```

**14. ワーカノード上で、マスタサーバ用のローカルDNS エイリアスを追加しましょう。/etc/hostsファイルを編集し、マスタのIP アドレスを追加してk8scp という名前を割り当てます。**
```
root@setX-worker:~# vim /etc/hosts

10.128.0.3 k8scp        #<-- この行を追加  
127.0.0.1 localhost
...
```

**15. トークンとハッシュ（この場合はsha256:hash）を利用して、ワーカとなる2 つ目のノードからクラスタにjoin しましょう。コントロールプレーンサーバのプライベートなIP アドレスと6443 番ポートを利用します。コントロールプレーンノードのkubeadm init の出力内容に例がありますので、残っていれば使用しましょう。**
```
root@setX-worker:~# kubeadm join --token 27eee4.6e66ff60318da929 k8scp:6443 --discovery-token-ca-cert-hash sha256:6d541678b05652e1fa5d43908e75e67376e994c3483d6683f2a18673e5d2a1b0


[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm
,→ kubeadm-config -oyaml'
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file
,→ "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Activating the kubelet service
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...
This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.
Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
  
```

**16. ワーカノードでkubectl コマンドを実行します。失敗するはずです。その理由は、ローカルの.kube/configファイルにクラスタと認証キーがないためです。**
```
root@setX-worker:~# exit
student@setX-worker:~$ kubectl get nodes

The connection to the server localhost:8080 was refused  
- did you specify the right host or port?
```
  
```
student@setX-worker:~$ ls -l .kube  

ls: cannot access '.kube': No such file or directory  
```



以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/Lab3-3.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)
