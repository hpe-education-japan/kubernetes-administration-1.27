# 課題3.1: Kubernetes のインストール


**概要**

さまざまなベンダがKubernetes のインストールツールを提供しています。その一つであるkubeadm の使い方をこの演習で学びます。kubeadm はKubernetes クラスタを構築する方法の主流になると期待されており、コミュニティがサポートする独立ツールです。  

**プラットフォーム: Digital Ocean, GCP, AWS, VirtualBox, etc**

本講座の演習は、Google Cloud Platform (GCP) 上で起動した Ubuntu 20.04 インスタンスを使って記述しています。柔軟性の観点から、特定のベンダに依存しないように記述しています。AWS でもローカルハードウェアでも仮想マシンでも実行できます。アクセス方法や注意事項はプラットフォームによって違います。VirtualBox については、v1.21.0 以降では（最低限稼働する）最小サイズは、コントロールプレーンは 3vCPU、メモリ 4GB、ディスク 5GB、最小インストールの OS、またワーカノードは 1vCPU、メモリ 2GB、ディスク 5GB、最小インストールの OS となります。その他大抵のプラットフォームでは、2CPU、メモリ7.5G で動作します。    
<br>

ローカルマシンを利用しているなら、各ノードで swap を無効にし、またネットワークインタフェースを一つのみにする必要があります。
複数インタフェースもサポートされていますが、追加の設定が必要です。kubeadm コマンドの使用中、警告やエラーとして表示される
他の要件もあるかもしれません。多くのコマンドは普通のユーザとして実行しますが、root 権限を要するコマンドもあります。先の演習
で紹介した手順に沿って sudo アクセスを設定します。GCP や AWS を通してノードにリモートアクセスする場合、ローカルターミナル
や PuTTY のような SSH クライアントが必要になります。PuTTY はwww.putty.orgからダウンロードできます。ノードにアクセスするに
は、.pemファイルあるいは.ppkファイルも必要になります。それぞれのクラウドプロバイダにこのファイルをダウンロードまたは作成す
るプロセスがあります。講師と対面で受講している場合は、講義中にファイルが提供されるでしょう。  

**重要**

Kubernetes の学習中はすべてのファイアウォールを無効にしてください。コンポーネント間の通信に要求されるポート番号リストがありますが、このリストは不完全な場合もあります。GCP を使用している場合は、すべてのポートへのすべての通信を許可するルールをプロジェクトに追加できます。  VirtualBox を使用している場合は、VM 間のネットワークをpromiscuous mode に設定する必要がある点に注意してください。  
<br>
次の問題では、Kubernetes をシングルノードにインストールしてから、計算リソースの追加でクラスタを成長させます。2 つのノードの大きさは同じで、vCPU は2 つでメモリは4G です。より小さなノードの利用も可能ですが、処理速度は低下し、予期せぬエラーが発生する可能性があります。  

**YAML ファイルとホワイトスペース**

いくつかの演習では、この教材に含んでいるYAML ファイルを使用します。可能なときは、ファイルを自分で書くことをおすすめします。YAML の書式には、空白やインデントに関して覚えた方がよいルールがあるからです。重要な注意点として、YAML ファイル内ではタブを使用しないでください。インデントの仕様で、スペースのみ使用可能です。  
<br>
PDF を使用している場合は、コピーして貼り付けする場合、通常シングルクォート（'）が正しく反映されずに、バッククォート（`）になってしまいます。そのためこれは手作業で直す必要があります。YAML ファイルは圧縮してtar ファイルとしても載せています。こちらのURL でこれらのリソースを確認できます:  
https://training.linuxfoundation.org/cm/LFS458-JP  
ログインする際は、ユーザ名：LFtraining   パスワード：Penguin2014 を利用してください。  
<br>
現在のファイルの名前とリンク（講座が新しくなると変わります）を見つけたら、次のようにコマンドラインでwget を実行してノードにファイルをダウンロードし、展開します:  
```
$ wget https://training.linuxfoundation.org/cm/LFS458/LFS458_V1.28.1u2_SOLUTIONS.tar.xz \
--user=LFtraining --password=Penguin2014

$ tar -xvf LFS458_V1.28.1u2_SOLUTIONS.tar.xz
 
```
(注意: 使用しているPDF ビューアによっては前述のコマンドをカット＆ペーストすると、アンダーライン（「_」）の記号がスペースに変換されてしまう場合があります。その場合は手動でコマンドラインを修正します)  

<br>
<br>

## Kubernetes をインストール

コントロールプレーンノードにログインします。講師と対面で受講している場合は、ノードの IP アドレスを講師から受け取ってく
ださい。アクセスするには、.pemあるいは.ppkのキーが必要になります。どちらが必要であるかは、ターミナルで ssh を利用し
ているのか PuTTY を利用しているのかによります。キーは講師が提供します。  

**1. 1 つ目のノードでターミナルセッションを開きます。**

例えば、1 つ目のGCP ノードにPuTTY あるいはSSH セッションを通して接続します。例ではユーザ名student を利用していますが、違うユーザ名かもしれません。非 root ユーザがなければ作成して下さい。また、例の中の IP アドレスとあなたが利用する IP アドレスも違うでしょう。pem あるいは ppk 鍵のアクセスモードの変更
が必要かもしれません。  
```
[student@laptop ~]$ chmod 400 LFS458-JP.pem
[student@laptop ~]$ ssh -i LFS458.pem student@35.226.100.87

The authenticity of host '54.214.214.156 (35.226.100.87)' can't be established.  
ECDSA key fingerprint is SHA256:IPvznbkx93/Wc+ACwXrCcDDgvBwmvEXC9vmYhk2Wo1E.  
ECDSA key fingerprint is MD5:d8:c9:4b:b0:b0:82:d3:95:08:08:4a:74:1b:f6:e1:9f.  
Are you sure you want to continue connecting (yes/no)?yes  
Warning: Permanently added '35.226.100.87' (ECDSA) to the list of known hosts.  
< 省略>
```
**2. 先の wget コマンドを使って、講座用のアーカイブをノードにダウンロードし、展開します。コピーして貼り付けする場合、アンダ
ースコアが貼り付けされない場合があります。**  

**3. PDF あるいは eLearning を利用している場合は、コピー・ペーストを使わず、コマンドを打ち込むことを推奨します。コマンドを
打ち込むことで、コマンドおよびその意味の両方を覚えることができます。長いハッシュや出力をコピーするほうが遥かに簡単、
といった例外は有りますが、学びにはなりません。**

**4. root になってシステムのアップデートとアップグレードを行います。いくつか入力が必要となるかもしれません。その場合、再起
動を許可し、現在ローカルにインストールされているバージョンを維持します。まず yes を入力し、それから 2 を入力します。**

基本的に入力は求められませんが、いくつか必要となるかもしれません。必要なら、再起動を許可し、現在ローカルにインストールされているバージョンを維持します。まずyes を入力し、それから2 を入力します。  
```
student@setX-cp:~$ sudo -i
root@setX-cp:~# apt-get update && apt-get upgrade -y

< 省略>  
You can choose this option to avoid being prompted; instead,  
all necessary restarts will be done for you automatically  
so you can avoid being asked questions on each library upgrade. 
```
```
Restart services during package upgrades without asking? [yes/no] yes

< 省略>  
A new version (/tmp/fileEbke6q) of configuration file /etc/ssh/sshd_config is  
available, but the version installed currently has been locally modified.  
  
1. install the package maintainer's version  
2. keep the local version currently installed  
3. show the differences between the versions  
4. show a side-by-side difference between the versions  
5. show a 3-way difference between available versions  
6. do a 3-way merge between available versions  
7. start a new shell to examine the situation  

What do you want to do about modified configuration file sshd_config? 2

< 省略>
```

**5. nano(使い勝手の良いエディタ)、vim、emacs のようなテキストエディタをインストールします。いずれも使えますが、この演習で
は vim を使います。**
```
root@setX-cp:~# apt-get install -y vim

< 省略>
```

**6. コンテナ環境の主な選択肢は containerd、cri-o そして Docker です。この講座では、構築が簡単なこととクラウドプロバイダにて広く利用されていることから、containerd を推奨します。**

エンジンは一つだけインストールしてください。複数のエンジンがインストールされている場合、kubeadm の初期化プロセスでは、検索パターンのために Docker を利用します。containerd 以外のエンジンを利用した場合は、いくつかのコマンドで異なる出力が表示されるかもしれないことに注意して下さい。    

**7. 全ての依存関係を確保するため、いくつかのパッケージをインストール必要があります。バックスラッシュは必ずしも必要ではなく、一行で入力する場合は省略可能です。**
```
root@setX-cp:~# apt install curl apt-transport-https vim git wget \
software-properties-common lsb-release ca-certificates -y

< 省略 >
```

**8. swap を無効化していない場合は、無効化してください。クラウドプロバイダーは、swap をイメージ内で無効化しています。**
```
root@setX-cp:~# swapoff -a
```

**9. 下記の手順により、モジュールが利用可能になるよう、ロードします。**
```
root@setX-cp:~# modprobe overlay

root@setX-cp:~# modprobe br_netfilter
```

**10. Kernel のネットワークを、必要なトラフィックを許可するように更新します。復帰文字の後もコマンドが続くことを示すため、シェルが大なり記号 (>) を付加することに注意して下さい。**
```
root@setX-cp:~# cat << EOF | tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
```

**11. 変更が現在のカーネルでも利用されるようにします**
```
root@setX-cp:~# sysctl --system

* Applying /etc/sysctl.d/10-console-messages.conf ...
kernel.printk = 4 4 1 7
* Applying /etc/sysctl.d/10-ipv6-privacy.conf ...
...
net.ipv6.conf.all.use_tempaddr = 2
net.ipv6.conf.default.use_tempaddr = 2
* Applying /etc/sysctl.d/10-kernel-hardening.conf ...
kernel.kptr_restrict = 1
< 省略 >
```

**12. ソフトウェアのインストールに必要な鍵をインストールします**
```
root@setX-cp:~# sudo mkdir -p /etc/apt/keyrings
root@setX-cp:~# curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
| sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

root@setX-cp:~# echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

```

**13. containerd をインストールします**
```
root@setX-cp:~# apt-get update && apt-get install containerd.io -y
root@setX-cp:~# containerd config default | tee /etc/containerd/config.toml
root@setX-cp:~# sed -e 's/SystemdCgroup = false/SystemdCgroup = true/g' -i /etc/containerd/config.toml
root@setX-cp:~# systemctl restart containerd


Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required:
< 省略 >
```

**14. Kubernetes 用に新しいリポジトリを追加します。**

tar ファイルをダウンロードするか、GitHub にあるコードする利用することもできます。ファイルを作成して、利用するディストリビューション用のメインリポジトリを入力します。  
※このリポジトリにはkubernetes 1.27のみが含まれます。  
```
root@setX-cp:~# echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.27/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

```

**15. パッケージ用にGPG キーを追加します。**
```
root@setX-cp:~# curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.27/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

```  

**16. 記述したリポジトリを更新します。これにより、新しく追加したリポジトリ情報をダウンロードするようになります。**
```
root@setX-cp:~# apt-get update

< 省略>
```

**17. Kubernetesソフトウェアをインストールします。**

頻繁にリリースされているので、コマンドラインでイコールサイン (=) とバージョン情報を省けば、最新のリリースをインストールできます。最新のバージョンは通常、変更が多く、複数のバグが潜在する可能性があります。そのため、最近リリースされた安定版で固定します。後の演習で、クラスタをより新しいバージョンにアップグレードします。   

```
root@setX-cp:~# apt install -y kubeadm kubelet kubectl

< 省略>
```
```
root@setX-cp:~# apt-mark hold kubelet kubeadm kubectl  

kubelet set on hold.  
kubeadm set on hold.  
kubectl set on hold.
```

**18. コントロールプレーンサーバのプライマリインターフェイスのIP アドレスを探します。**

次の例では、ens4 インターフェイスで、IP アドレスは10.128.0.3 です。あなたの場合は異なっている可能性があります。IP アドレスを探す方法は 2 つあります。  
    
```
root@setX-cp:~# hostname -i

10.128.0.3
```
```
root@setX-cp:~# ip addr show

....  
2: ens4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1460 qdisc mq state UP group default qlen 1000  
link/ether 42:01:0a:80:00:18 brd ff:ff:ff:ff:ff:ff  
inet 10.128.0.3/32 brd 10.128.0.3 scope global ens4  
valid_lft forever preferred_lft forever  
inet6 fe80::4001:aff:fe80:18/64 scope link  
valid_lft forever preferred_lft forever  
....  
```

**19. コントロールプレーンサーバー用のローカルDNS エイリアスを追加します。/etc/hostsファイルを編集し、先のIP アドレスを追加してk8scp という名前を割り当てます。**
```
root@setX-cp:~# vim /etc/hosts

10.128.0.3 k8scp       #<-- この行を追加
127.0.0.1 localhost
....
```

**20. クラスタ用の設定ファイルを作成しましょう。***

多くのオプションが含まれていますが、containerd、Docer、cri-oで異なります。講座のアーカイブに含まれているファイルを利用して下さい。クラスタの初期化後、他に使われているデフォルトの値を見てみましょう。IP アドレスではなく/etc/hostsに追加したノードのエイリアス名を使ってください。そうすると、今後の演習でロードバランサをデプロイしたときもネットワーク証明書が効果を持ち続けます。ファイルは口座のアーカイブ内にもあります。  

```
root@setX-cp:~# vim kubeadm-config.yaml

```
![chap3-10](/uploads/a18123c0a930bdae1a5b0f871d34b53e/chap3-10.png)

[kubeadm-config.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/labfiles/kubeadm-config.yaml)
 

**21. コントロールプレーンノードを初期化し、出力内容を一行ずつ読みましょう。**

Kubernetes のバージョンによっては出力が異なっているかもしれません。最後の方に、root 以外のユーザでKubernetes を操作するための手順を記載しています。トークンも記載しています。この情報は、後からでもkubeadm token list コマンドで参照できます。この出力内容では、次の手順であるクラスタ用のPod ネットワークを作成することも指示しています。先の手順で閲覧した設定ファイル内のCalico のネットワーク設定をコマンドの引数に渡します。  
注意: このコマンドは、後の演習で実施しなければいけないコマンドをいくつか出力します。    
```
root@setX-cp:~# kubeadm init --config=kubeadm-config.yaml --upload-certs | tee kubeadm-init.out 

(teeを使い、のちの確認のために出力を保存します。)

[init] Using Kubernetes version: v1.27.1
[preflight] Running pre-flight checks

< 省略 >

You can now join any number of the control-plane node
running the following command on each as root:
kubeadm join k8scp:6443 --token vapzqi.et2p9zbkzk29wwth \
--discovery-token-ca-cert-hash
,→ sha256:f62bf97d4fba6876e4c3ff645df3fca969c06169dee3865aab9d0bca8ec9f8cd \
--control-plane --certificate-key
,→ 911d41fcada89a18210489afaa036cd8e192b1f122ebb1b79cce1818f642fab8
Please note that the certificate-key gives access to cluster sensitive
data, keep it secret!
As a safeguard, uploaded-certs will be deleted in two hours; If
necessary, you can use
"kubeadm init phase upload-certs --upload-certs" to reload certs afterward.
Then you can join any number of worker nodes by running the following
on each as root:
kubeadm join k8scp:6443 --token vapzqi.et2p9zbkzk29wwth \
--discovery-token-ca-cert-hash
,→ sha256:f62bf97d4fba6876e4c3ff645df3fca969c06169dee3865aab9d0bca8ec9f8cd
```

**22. 先の出力の最後で提案があった方法で、root 以外のユーザにクラスタへの管理者レベルでのアクセスを許可します。設定ファイルを複製し、パーミッションを修正したら、設定ファイルを確認します。**
```
root@setX-cp:~# exit
logout
student@setX-cp:~$ mkdir -p $HOME/.kube
student@setX-cp:~$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
student@setX-cp:~$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
```
student@setX-cp:~$ less .kube/config

apiVersion: v1  
clusters:  
- cluster:  
< 省略>
```

**23. Container Networking Interface (CNI) にどのポッド ネットワークを使用するかを決定するには、クラスターで予想される需要を考慮する必要があります。 CNI-Genie プロジェクトはこれを変更しようとしていますが、クラスタごとに存在できるポッド ネットワークは 1 つだけです。**

ネットワークでは、コンテナー間、ポッド間、ポッドからサービス、および外部からサービス間の通信が可能である必要があります。
今回Cilium をネットワーク プラグインとして使用します。これにより、コースの後半でネットワーク ポリシーを使用できるようになります。 現在、Cilium はデフォルトの CNI ではありません。  

Cilium は通常、「cilium install」または「helm install」コマンドを使用してインストールされます。 今回は、以下のコマンドを使用して作成した cilium-cni.yaml ファイルを使います。以下のコマンドを実行する必要はありません。参照用です。  

$ helm repo add cilium https://helm.cilium.io/  
$ helm repo update  
$ helm template cilium cilium/cilium --version 1.14.1 \  
--namespace kube-system > cilium.yaml  
<br>

以下のようにcilium-cni.yaml使ってインストールします。

```
student@setX-cp:~$ find $HOME -name cilium-cni.yaml
student@setX-cp:~$ kubectl apply -f /home/student/LFS458/SOLUTIONS/s_03/cilium-cni.yaml


serviceaccount/cilium created
serviceaccount/cilium-operator created
secret/cilium-ca created
configmap/cilium-config created  
< 省略> 
```

**24. 多くのオブジェクトは短い名前ですが、kubectl の入力は少し大変かもしれません。bash の自動補完を有効にしましょう。**

まず、現在のシェルに設定を追加します。自動補完が永続的になるように˜/.bashrcファイルを更新します。bash-completion パッケージがインストールされていることを確認してください。インストールしたら、シェル補完が有効になるように一旦ログアウトしてからログインし直します。  
```
student@setX-cp:~$ sudo apt-get install bash-completion -y
```
< ログアウトしてログインし直します>  
```
student@setX-cp:~$ source <(kubectl completion bash)
student@setX-cp:~$ echo "source <(kubectl completion bash)" >> ~/.bashrc
```

**25. もう一度Node をdescribe するコマンドを実行し、自動補完が有効であることを確認しましょう。**

サブコマンドの最初の3 文字を入力してからTab キーを押します。自動補完はdefault namespace を想定します。別のnamespace で自動補完を使用するためには、最初にnamespace を入力しましょう。Tab を何度も押すと、可能な値のリストが表示されます。必要な名前が使用できるまで入力を続けます。まず現在のノードを確認し(あなたのノード名はcpで始まっていないかもしれません)、次にkube-system 内のPod を確認しましょう。  
もしPod 一覧の代わりに -bash: _get_comp_words_by_ref: command not found のようなエラーが表示されたら、前の手順に戻って、必要なソフトウェアをインストールし、ログアウトしてからログインし直します。  
```
student@setX-cp:~$ kubectl des<Tab> n<Tab><Tab> cp<Tab>
student@setX-cp:~$ kubectl -n kube-s<Tab> g<Tab> po<Tab>
```

**26. kubeadm help コマンドを調べます。コマンドからの出力は省略しています。ヘルプトピックを確認しましょう。**
```
student@setX-cp:~$ kubectl help

student@setX-cp:~$ kubectl help create
```

**27. クラスタを作成するときのkubeadm-config.yamlファイルに含まれている他の値を見てみましょう。**
```
student@setX-cp:~$ sudo kubeadm config print init-defaults

apiVersion: kubeadm.k8s.io/v1beta3
bootstrapTokens:
- groups:
  - system:bootstrappers:kubeadm:default-node-token
  token: abcdef.0123456789abcdef
  ttl: 24h0m0s
  usages:
  - signing
  - authentication
kind: InitConfiguration
 < 省略>
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/Lab3-2.md)

[Top](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27)