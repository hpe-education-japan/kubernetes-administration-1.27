# Kubernates Administration Lab
<br>

## Chapter2 Kubernetes の基本
[・Chapter2-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter2/Lab2.md)  
## Chapter3 インストールと設定
[・Chapter3-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/Lab3-1.md)  
[・Chapter3-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/Lab3-1.md)  
[・Chapter3-Lab3](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/Lab3-3.md)  
[・Chapter3-Lab4](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/Lab3-4.md)  
[・Chapter3-Lab5](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter3/Lab3-5.md)  
## Chapter4 Kubernetes アーキテクチャ  
[・Chapter4-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter4/Lab4-1.md)  
[・Chapter4-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter4/Lab4-2.md)  
[・Chapter4-Lab3](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter4/Lab4-3.md)  
## Chapter5 API とアクセス  
[・Chapter5-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter5/Lab5-1.md)  
[・Chapter5-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter5/Lab5-2.md)  
## Chapter6 API オブジェクト  
[・Chapter6-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/Lab6-1.md)  
[・Chapter6-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/Lab6-2.md)  
[・Chapter6-Lab3](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter6/Lab6-3.md)  
## Chapter7 Deployment による状態管理  
[・Chapter7-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-1.md)  
[・Chapter7-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-2.md)  
[・Chapter7-Lab3](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-3.md)  
[・Chapter7-Lab4](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-4.md)  
[・Chapter7-Lab5](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter7/Lab7-5.md)   
## Chapter8 Volume と Data  
[・Chapter8-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/Lab8-1.md)  
[・Chapter8-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/Lab8-2.md)  
[・Chapter8-Lab3](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/Lab8-3.md)  
[・Chapter8-Lab4](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter8/Lab8-4.md)  
## Chapter9 Service  
[・Chapter9-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter9/Lab9-1.md)  
[・Chapter9-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter9/Lab9-2.md)  
[・Chapter9-Lab3](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter9/Lab9-3.md)  
[・Chapter9-Lab4](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter9/Lab9-4.md)  
## Chapter10 Helm  
[・Chapter10-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter10/Lab10-1.md)    
## Chapter11 Ingress  
[・Chapter11-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter11/Lab11-1.md)  
[・Chapter11-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter11/Lab11-2.md)  
## Chapter12 スケジューリング  
[・Chapter12-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter12/Lab12-1.md)  
[・Chapter12-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter12/Lab12-2.md)  
## Chapter13 ロギングとトラブルシューティング  
[・Chapter13-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter13/Lab13-1.md)  
[・Chapter13-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter13/Lab13-2.md)  
[・Chapter13-Lab3](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter13/Lab13-3.md)  
## Chapter14 Custom Resource Definition  
[・Chapter14-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter14/Lab14-1.md)  
## Chapter15 セキュリティ  
[・Chapter15-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter15/Lab15-1.md)  
[・Chapter15-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter15/Lab15-2.md)  
[・Chapter15-Lab3](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter15/Lab15-3.md)  
## Chapter16 High Availability  
[・Chapter16-Lab1](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter16/Lab16-1.md)  
[・Chapter16-Lab2](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.27/-/blob/main/Chapter16/Lab16-2.md)  




